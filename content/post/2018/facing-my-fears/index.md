+++
author = "Anthony"
categories = ["judo", "sports"]
date = 2018-02-10T05:06:32Z
description = ""
draft = false
slug = "facing-my-fears"
tags = ["judo", "sports"]
title = "Facing my fears"

+++


I injured my knee 2 weeks ago during Judo class due to someone new doing a technique improperly. I have been planning to go to the San Jose judo tournament for a while, and the injury has made me unsure about whether I should spend money signing up, driving 6 hours each way, and booking a hotel overnight. I stopped going to practice to let it rest and it felt better a week later but it still feels tight and there's lingering pain after walking my dog for more than a few blocks. So I made an appointment to see a doctor and let him decide.

The few days leading up to the appointment, many questions and worries kept popping up in my mind. 

*"What if he says I need a surgery? that's a lot of money."*

*"I already told my Sensei that I'd be competing would I be disappointing every one by not going?"*

The doctor ended up saying I sprained my MCL and there shouldn't be a tear, but there isn't anyway to tell without doing an MRI. He said I should be fine going to the tournament but take it easy and stop if I feel any pain. I signed up for the tournament that day.

In the following days, those worries and questions has turned into fears. What if my knee breaks? I've lost a lot of muscle weight and stamina since my last competition and dropped a weight class. This tournament has more, better and younger competitors than my previous ones, will I get thrashed and lose? I started thinking to myself, maybe I should just not show up and wait for the next tournament once my knee recovers and I'm back in shape. At this point I realized I'm trying to come up with excuses to not go, and nobody can/will stop me other than myself. This is also when I realize that my knee has already recovered a lot even if it's not at 100% and most of it is just my mind playing tricks on me to convince myself not to go.

I've encountered these situations before at work and in my personal life. I've gotten pretty good at just forcing myself to go through with things and thinking of what the worst case scenario that can happen. In this case, the worst case is I break my knee, but I think the chances of that happening wouldn't be much more than if I went in before the injury. In the end all it takes is a bad throw. The second worst case scenario, is that I just lose all my matches.

This is when I realized the root of my fear is not injuring my knee. I'm afraid of losing. I've been nervous before for my past tournaments, but I was never afraid of losing until now. I think now that I've been promoted I expect a certain level of performance and success from myself, and losing would ultimately mean I'm undeserving of the belt promotion in my mind. I'll disgrace my Sensei who has given me the belt. This is a form of pride and ego. I'm sure every one has heard about how the ego hinders your growth. I thought I understood this when I heard it from my Wing Chun Sifu, but here it is in directly trying to get me to miss this opportunity for growth and experience. Breaking down your ego is an on going process that never ends.

I'm not as strong or as fast as before and I might be competing while injured. But there is still things that I could learn and experiences to encounter. I can't avoid all my problems and opportunities in life just because things aren't going the way I want and I'm not 100% ready.

I'll make an updated post once I come back from the tournament this weekend.


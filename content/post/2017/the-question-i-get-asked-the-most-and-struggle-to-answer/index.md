+++
author = "Anthony"
categories = ["rant", "technology"]
date = 2017-06-22T12:53:40Z
description = ""
draft = false
slug = "the-question-i-get-asked-the-most-and-struggle-to-answer"
tags = ["rant", "technology"]
title = "The question I get asked the most and struggle to answer"

+++


I get asked this question fairly often, and it's one of those questions I had no problem answering earlier in my life, but as time went on I'm finding it more and more difficult to answer. I get asked this question in all kinds of social situations, from family gatherings to parties and meetups/conferences.

### *"What do you do?"*

The answer used to just be, student. Then right after college, the default answer was Consultant, or Software Engineer. Shortly after, I was a Systems Engineer, then I was a Software Engineer again. But now with some experience under my belt, people wanted to know what I **actually** do. Am I a PHP developer? A Java developer? iOS Engineer? Do I do frontend work with javascript? A Data Engineer?

The only appropriate response I can come up with, is I do everything, I used to reply with jack of all trades but people tend to overestimate my abilities when I say that and consider me a full stack engineer. My imposter syndrome has kicked in and I stopped calling myself that. My first year as a developer, our Information Assurance Officer got fired and they wanted me to fill in the role temporarily until they found a replacement. I found myself stuck doing part time development work and part time ensuring security controls are met and all of the massive amounts of paperwork that comes along with it. This went on for a year until they finally found someone to fill the role. It took a year of complaining, but I'm pretty sure they wanted me to keep doing that since my bill rate as a fresh grad was much lower than hiring an experienced person. Shortly after I was asked to help fill a Systems Engineer role since they were short handed. My time was split between dev and ops from that point for most of my career since then. During the government budget cuts, a lot of our testers and requirements analyst were let go from our project, and I found myself doing both roles for a while too (yes we were testing each others code).

### *"What have you worked on?"*

This question eventually gets asked at interviews and conversations with people that last longer than a few minutes. Usually this would be my chance to elaborate what I mean by I do everything, but being a government contractor means I'm unable to disclose the details of what I worked on. The explanation basically gets watered down to fixing lots of bugs, and maintaining systems that are a decade old, from the code to the database, to the infrastructure. Not a very good way to impress people especially since I don't end up writing many lines of code, and spend most of my time debugging issues and making sense of spaghetti code written by someone 8 years ago. People want to hear about people who work with the latest technology or the big feature they worked on.

Nobody really wants to hear about all the hacky solutions you've had to come up with to solve a simple issue made extremely complex by limitations of government regulation/budgets and bureaucratic inefficiencies. Telling people about all the thoughts and planning that goes into deploying to production via DVDs usually doesn't really go well in a day and age where rapid deployments to the cloud is the norm. I also cringe at the thought of telling people what our test coverage is. 

### What am I?
I find myself even more confused now as to what I am. My title is still a Software Engineer but I've found myself writing less and less code for work. But I've become more and more involved with sprint planning and estimating. I've trained my fair share of replacements for various projects and I've helped out many junior developers, but they are all writing more code than me now. I've been attempting to automate a lot of our internal deployment processes lately, but its been a slow uphill battle for the same reasons I've mentioned above.

I've discovered lately, that I just enjoy solving problems. Whether they are work related, non work related, technical, organizational or cultural, and I'm good at it. I don't think they have a job title for that, and even if they do I think it's hard to convince someone that skill is valuable, compared to someone who has 7 years of Javascript written on their resume who probably white boards and codes better than you.


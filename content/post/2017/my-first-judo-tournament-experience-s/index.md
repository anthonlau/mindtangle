+++
author = "Anthony"
categories = ["judo", "sports"]
date = 2017-05-24T22:33:04Z
description = ""
draft = false
slug = "my-first-judo-tournament-experience-s"
tags = ["judo", "sports"]
title = "My first judo tournament experience(s)"

+++


As I'm lying in bed still sore while typing this, I want to go back and compare my judo experiences back then and how I got started, with now and how my view / interest in judo has changed since I picked it back up again. I had my first judo tournament of my adult life 2 days ago. My very first judo tournament was actually right before I quit judo for the first time as a kid. 

## How it began
I recall telling my mom I wanted to learn Tae Kwon Do in Primary 5. Watching my friends practice in the assembly hall after school has gotten me interested in it. Instead, my mom made me learn Judo saying that its much safer cause you're not getting hit. She also doesn't want me getting into fights and punching other kids.

A year into learning Judo, I asked one of my friends who learns Tae Kwon Do to spar with me, I took him down with an [osotogari](https://www.youtube.com/watch?v=mgjfBnTMn1c) and proceeded to put him into a [kesa gatame](https://en.wikipedia.org/wiki/Kesa-gatame) hold, my friend just started punching me in the face until I let go of the hold. I could not understand why punching wasn't allowed in judo and thought I was wasting my time learning it. When I got home I told my mom I had no interest in judo and wanted to quit. I could not appreciate or understand the countless hours we spent practicing Ukemi (breakfalls) and slapping the mat along with the throws I saw as useless. She made me sign up for the local junior tournament before making the decision. At the time she also promised that she would buy me the game of life board game I've been wanting. Armed with only knowing Osotogari, [Ippon+Morote Seoi Nage](https://en.wikipedia.org/wiki/Ippon_seoi_nage) I went to my first tournament.

My first and only match was against a girl that was from my dojo and was around 4 years older than me, probably a foot and half taller and was also few kyu grades above me. She took me down with an osotogari, I remember being pinned down by her kesa gatame after, and as I tried for the first few seconds to get out of the hold with no progress at all, I looked over at the clock that started counting down towards my loss. I remember thinking, why should I even try? She's bigger than me, older than me and more experienced than me, how would would I even get out from under someone who is heavier than me. This wasn't a fair matchup in the first place. If only I could punch her in the face right now. I won't have to do Judo again after this anyways. I looked at the clock again, and accepted my loss quietly. I remember my sensei at the time was disappointed when I told him I'm quitting, since I was the 3rd and last person from my family to quit judo.

## Take two
Now, a good two decades later. I think it was during the 2016 Rio Olympics when I decided to start judo again. I've always watched Judo in the Olympics but for some reason this year, something just made me say to myself "hey, I should give judo another shot". Hopefully it's not the leg grab ban. Fast forward, 7 months later I'm attending the Nagase Cup in Fort Worth Texas.

One of the biggest difference is that I don't recall much from any of my matches. My memory is all a blur. As a kid I probably didn't care much about winning or losing and wasn't even trying my best. I felt like I was going into my first match with the same mindset, but as soon as I got pinned down a switch inside me just got flipped. I don't think I've every tried to physically exert that much strength in my life, not even when lifting my one rep max in the weight room. I got pinned over and over again, but I broke the pin over and over again and held onto my lapel with my dear life while I prayed that I would hear the word Mate(stop). I remember feeling faint and unable to feel my arms and hands every time I got up, and dreaded it every time the referee yelled out HAJIME(begin). I was gripping out of muscle memory at that point. Don't remember anything after that, I just remember hearing Ippon and I was announced the winner. Longest 4 minutes of my life.

I felt nauseous after the first match and was lying against the wall in a corner, contemplating just giving up on my remaining matches. Luckily after re hydrating and resting for a bit the nauseousness went away and I finished my remaining matches which I lost. I don't remember how I lost, but I remember wishing I still had the strength to give it my all to see how far I would be able to go.

## After thoughts
I still have a lot of work to do in terms of conditioning, ground work, and grip fighting. I would've probably gassed out before I had a chance to win my first match without my Muay Thai conditioning and Sensei Nick's brutal training regimen. I'll probably have to find more time to show up to class more often somehow and / or replace the time spent conditioning on drilling the more technical skills and ground transitions. I'm considering getting my deviated septum fixed which should improve my stamina significantly. I also realized my body is able go past it's limit for a short period of time, I'll have to discover how to harness and efficiently utilize it. The fact that I don't remember much about my matches means I have a long way to go in terms of mental training, I resorted to using my instincts and brute strength instead of having full control over my actions and decisions. I let my fear of falling, losing and failing take over.

Overall it was a great experience, I got to meet a lot of great people. The judo community is small, tight knit and take care of each other for the most part. It was fascinating watching everyone try their very best under pressure, the atmosphere is different from [randori](https://en.wikipedia.org/wiki/Randori) during practice. As Sensei Glenn would say, "One shiai(match) is worth a thousand randori sessions"


+++
author = "Anthony"
categories = ["food", "austin", "restaurant", "japanese food", "review"]
date = 2015-09-05T11:41:22Z
description = ""
draft = false
slug = "restaurant-review-fukumoto"
tags = ["food", "austin", "restaurant", "japanese food", "review"]
title = "Restaurant Review: Fukumoto, the Izakaya Austin Deserves?"

+++


I have been moving into my new apartment over the past week, and the movers came today to move all of our big furniture ( [Einstein Moving was AWESOME btw I highly recommend them](http://einsteinmoving.com/) ). I have also been eagerly anticipating the opening of Fukumoto, which is an [Izakaya](https://en.wikipedia.org/wiki/Izakaya) and sushi restaurant. I found out last week that it's only a few blocks away from our new apartment. After the movers left, we took a break and headed over expecting a huge wait since it opened only yesterday. To our surprise we were the only ones in the restaurant, and we were greeted with a loud [Irasshaimase!!!](http://www.turningjapanese.org/2014/07/what-does-irasshaimase-actually-mean.html)

## The Ambience
![Ambience](images/IMG_20150904_175346.jpg)
When I first stepped in it was totally different from what I expected. I expected a small to medium sized cramped, dark, restaurant with an American bar like atmosphere (after all Izakayas are places for people to drink after work). The decor and atmosphere made it seem closer to a higher end Japanese restaurant. The specials were posted on the walls written in both Japanese and English, but it was so small that you'd have to be sitting on that one table underneath it to be able to read it. Good thing the specials were also written on the menu, so the ones on the wall served more as a decoration. 

![Pills](images/IMG_20150904_173542.jpg)
As we sat down, they placed a plate down with 2 pills on it. Our server added water to it and it expanded into two hand towels.

## The Menu
![Menu 1](images/IMG_20150904_174242.jpg)
![Menu 2](images/IMG_20150904_174245.jpg)
The menu was very simple, few pieces of paper with the names of Dishes in Japanese and English clipped onto a clipboard. It was a bit unorganized, for some reason there were two yakitori sections on two different pages being separated by another section. The way that the menu was organized and the font they used in English made it a bit difficult to read. It also lacked descriptions on what some of the items were. I was familiar with what the items were, but people unfamiliar with Japanese food might not know what some of the things were, and it would be quite annoying to ask your server what things were one by one.
![Menu Alcohol](images/IMG_20150904_174248.jpg)
![Alcohol](images/IMG_20150904_174835.jpg)
The sake selection was impressive for Austin. The last time I saw this many choices was in Japan and New York. I do wish there were more hot sake selections in America in general. I found it a bit weird that being a Japanese restaurant you don't carry Sapporo or Kirin. With that said, the sake and beer we got was recommended by the bartender and they were both amazing.

We were also told that they will be adding more to the menu in the future.

## The Food
You'll find many "exotic" dishes that many might be unfamiliar with, such as chicken heart and chicken gizzard yakitori. The [Yakitori](https://en.wikipedia.org/wiki/Yakitori) (grilled skewers over coal) over all were amazing. My one and only complaint is that many of the yakitori dishes seem to have a really intense flavor and it covers up the aroma that you get from charcoal grilling. Yakitori speciality restaurants usually use very expensive imported coal to grill their skewers, similar to how Americans smoke their meat.

![Sushi](images/IMG_20150904_175933.jpg)
I have similar complaints about the sushi though this is all my personal opinion and I'm not a sushi expert. The rice seems to be seasoned with rice vinegar a bit too heavily, it balanced well with stronger flavored fish, but the rice definitely overwhelmed some of the lighter flavored fish such as the yellowtail and snapper we had.

![Kama toro](images/IMG_20150904_175940.jpg)

For both sushi and yakitori, you will find some items here that aren't normally found in most Japanese restaurants. I personally try to avoid eating bluefin tuna for sustainability reasons, but I told my girlfriend to try the Kama toro which was on the menu since the only other place I've seen it served, which was the first and last time I've tried it was in Sushi Yasuda in New York. The [Kama Toro](http://www.sushiencyclopedia.com/blog/2007/10/14/the-secret-fatty-tuna-toro-sushi/) is like a super toro, the meat around the collar of the tuna. It is rarer and more expensive than regular toro ($18 for one piece), and the marbling was out of this world.

![Bacon Asparagus](images/IMG_20150904_175013.jpg)
Bacon Wrapped Asparagus

![Pork Belly](images/IMG_20150904_175041.jpg)
Pork Belly

![Chicken Heart](images/IMG_20150904_175549.jpg)
Miso Glazed Chicken Heart

![Tamago](images/IMG_20150904_181610.jpg)
Tamago (Japanese Omelette)

![Agedashi Tofu](images/IMG_20150904_181256.jpg)
This was the best Agedashi Tofu I have ever had in any restaurant, it was prepared perfectly.

![Chicken meatball](images/IMG_20150904_181816.jpg)
Chicken Meatball dipped in quail egg and Bonito shavings. This was a bit too heavy for me but my girlfriend loved it.

![Negitoro](images/IMG_20150904_182305.jpg)
Negitoro

![Monkfish Liver](images/IMG_20150904_183154.jpg)
Monkfish Liver

![Dessert](images/IMG_20150904_183150.jpg)
Some Dessert the girlfriend ordered

## Closing Thoughts
So I'll get this out of the way first, yes this place is expensive, but only if you come hungry and intend to get full. Each order comes in only 1 piece/skewer, and it was around roughly 3-5+ dollars each order. But Izakayas are meant to be small snack sized dishes you eat while drinking, a few of the yelp reviews I read complained about the portions and price. I don't think the complaints about the portions were justified but it is definitely a bit pricey, and Izakayas are meant to be affordable drinking establishments. Although I feel like in America it has kind of started to become a marketing term similar to fusion style. A lot of places I have been to in the past started calling themselves Izakayas, but in reality they are only serving low quality sushi, edamame and large selection of alcohol. There were a few authentic Izakaya's in New York, but I feel like Fukumoto can definitely be qualified as an Izakaya in my book. I won't be coming back for dinner (I'd go bankrupt), but I'd definitely be coming back very often for after dinner snacks and drinks (and ramen if they add that to their menu in the future).

### Good
* Uncommon ingredients
* Flavor was incredible
### Bad
* Price/portions (read above)
* Small menu (for now)
* No Sapporo / Kirin Beer
* Closes very early for being an Izakaya

**Presentation:** 4/5

**Quality:** 4/5

**Taste:** 4/5

**Price:** Expect to spend ~$100 per person if you're hungry and you plan to fill yourself up and order alcohol here.

**Price to Quality Rating:** 2/5 for dinner 4/5 for after dinner drinks + snacks

Fukumoto
514 Medina Street
Austin, TX 78702
512.770.6880
http://fukumotoaustin.com/

Hours:
Mon – Thurs 4:30pm-10:00pm
Fri & Sat – 4:30pm-11:00pm
Dining starts at 5pm.


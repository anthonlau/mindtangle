+++
author = "Anthony"
categories = ["food", "austin", "restaurant", "review", "bars"]
date = 2015-08-05T12:04:59Z
description = ""
draft = false
image = "/images/2015/08/IMG_20150731_191334-1.jpg"
slug = "restaurant-review-peche-austin"
tags = ["food", "austin", "restaurant", "review", "bars"]
title = "Restaurant Review: Peché"

+++


Decided to meet my girlfriend and a few of her coworkers downtown at Peché for drinks and dinner. I was told that their cocktails are amazing, but expensive. I found out later that it wasn't just the drinks even the food was expensive. The food was good but not amazing, and definitely not worth that price considering the portions are amazingly small. Good thing I had a late lunch that day.

There's really not much to say, my girlfriend ordered a burger with fries, and I ordered a Coriander duck breast that sounded amazing. I was expecting a regular sized duck breast you would get at any other restaurant but this was what I got. The picture makes it look bigger than it actually is. The flavor was good, but the duck was a bit rubbery and it defintely wasn't $28 good. Took a bite of my girlfriends burger and it's pretty standard too.

![Small duck breast](images/IMG_20150731_192925.jpg)

The cocktails on the other hand was definitely worth the price. I ordered a Gin and Tonic (one of my favorite drinks and what I usually order to compare places with). They also had a mixologist (I think that's what he's called, but basically a cocktail sommelier) going around talking to people answering their questions and recommending drinks. A drink called Barton Springs caught my eye since the ingredients resembled a Vesper, which is another favorite drink of mine. He told me that was one of his favorite drinks and boy was it good.

![Barton Springs](images/IMG_20150731_191334.jpg)

I would definitely come back here for drinks. Just a heads up though, they seem to be purely a cocktail bar / restaurant and they don't serve any beer.

**Presentation:** 4/5

**Quality:** 4/5 for food 5/5 for cocktails

**Taste:** 3/5 for food 5/5 for cocktails

**Price:** Expect to spend ~$30 per person with no appetizer, more if you have alcohol.

**Price to Quality Rating:** 2/5 for food 4/5 for cocktails

Peché
208 W 4th Street - ATX - 78701
512.494.4011

Hours:
Mon – Fri 4pm-late!
Sat & Sun – 5pm-late!


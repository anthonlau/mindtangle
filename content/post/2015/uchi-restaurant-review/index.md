+++
author = "Anthony"
categories = ["food", "austin", "restaurant", "japanese food", "review"]
date = 2015-07-23T14:19:23Z
description = ""
draft = false
image = "/images/2015/08/IMG_20150712_180255.jpg"
slug = "uchi-restaurant-review"
tags = ["food", "austin", "restaurant", "japanese food", "review"]
title = "Restaurant Review: Does Uchi live up to the hype?"

+++


*On to my first restaurant review on this blog. I think I'd like to set up a general guideline on writing my reviews. A brief introduction, followed by a list of Pros and Cons, and a closing thoughts.*

I love Japanese food and Japanese cuisine in general, and that is no secret to my friends and family. Since moving to Austin my girlfriend and everybody I have spoken to has told me that Uchi has the best Sushi in Austin, if you're willing to pay for it. I was told that I should make reservations in advance or expect to wait for a few hours. I've been so busy since moving to Austin that the only Japanese sushi restaurant we had tried was **[D K Sushi](http://www.yelp.com/biz/d-k-sushi-restaurant-austin)**. I had set my expectations for sushi in Austin pretty low and **D K Sushi** definitely exceeded the expectations I had and was very reasonably priced for the quality.  After giving into my craving for sushi one day I decided to call and make reservations for Uchi. I'll be comparing Uchi to **[Sushi Yasuda](http://sushiyasuda.com/)** a lot in this review since that was the only other restaurant I've been to in the US that sources their fish from Tsukiji Market and their price are similar.


**The Good**

* Good selection of sake
* They source their fish from all over the world and has a section on the menu listing where their fish come from (seems to not include every fish on their menu though)
* They have a selection of fish from Tsukiji market. On their website it says they fly in shipments from the Tsukiji market from Japan every day. They update their menu everyday based on what fish they have that is fresh. The last time I went to a restaurant that did that was at **[Sushi Yasuda](http://sushiyasuda.com/)** in New York City when I still lived there, and the price was a bit higher.
* Lots of interesting apetizers and dishes that either scream fusion or traditional Japanese prepared a bit differently with a twist.
* Offers [Omakase](https://en.wikipedia.org/wiki/Omakase)
* Fish was very high quality and everything tasted great and had great presentation
* The pork belly and the brussell sprouts were AMAZING. Must Try.(Picture of the pork belly is not the full portion, we attacked it before I remembered to take a picture)

![Tsukiji Menu](images/IMG_20150712_174612.jpg)
![Menu](images/IMG_20150712_174620.jpg)
![Brussel Sprouts](images/IMG_20150712_183606.jpg)
![Pork Belly](images/IMG_20150712_180219.jpg)

**The Not So Good**

* Selection of Tsukiji fish was small compared to Sushi Yasuda.
* The nigiri's fish and rice's temperature was a bit off, some pieces were too cold and some too warm.
* Compared to Sushi Yasuda, you can tell the difference between the sushi chefs skills with the texture of the fish the moment you put the piece of Nigiri into your mouth. I don't want to claim that I'm a sushi expert and I can't really describe the exact difference, but Sushi Yasuda's fish are cut a bit thinner without being paper thin throughout the whole piece. This causes the sushi to not feel like a chunky piece of meat that's just in your mouth and gives the fish a more organic and lively smooth texture. To clarify I'm not saying that Uchi are serving American sized portion brick shaped salmon and tuna. Sushi Yasuda's chef were all trained master sushi chefs from Japan, and I can just tell that the person who made my sushi probably isn't.
* Parking is a bit limited, but there is valet.

![Chutoro](images/IMG_20150712_183139.jpg)
![Saba](images/IMG_20150712_183427.jpg)
![Uni](images/IMG_20150712_183601.jpg)
![Karage](images/IMG_20150712_180547.jpg)

**The Bad**

* The restaurant was VERY loud, I had trouble hearing the people I was with and lost my voice by the end of the night trying to speak over the noise. This is very unusual for a Japanese restaurant that serves high quality fish and definitely a first time experience for me. It doesn't bother me but I wouldn't suggest someone to take their first date here if they intend to have a conversation.
* Only one selection of warm sake and it is the same sake that I use for cooking.
* Limited selection of traditional style rolls and fish, menu is mostly fusion dishes.

![Roll](images/IMG_20150712_180255.jpg)

I'm a fan of traditional style Sushi, no soy sauces, no fancy sauces. Just let the quality of the fish, the sushi chef's skill and the subtle flavors and texture do all the talking. All in all, I was impressed by Uchi and think it's a great place that you should definitely try. The price isn't ridiculous either for the quality that you're getting. But will I be coming back anytime soon? Probably not, I personally prefer to go to a sushi restaurant that has more choices of fish and traditional dishes done the traditional or even home cooked style way.

**Presentation:** 5/5

**Quality:** 5/5

**Taste:** 4/5

**Price:** Expect to spend ~$60 per person, more if you have alcohol.

**Price to Quality Rating:** 4/5

Uchi
801 S. Lamar Blvd
Austin, TX 78704
512.916.4808

Hours
Sun-Th 5-10, Fri-Sat 5-11


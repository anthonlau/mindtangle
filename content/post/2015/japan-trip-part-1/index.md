+++
author = "Anthony"
categories = ["travel", "food", "Japan"]
date = 2015-03-29T13:12:37Z
description = ""
draft = false
slug = "japan-trip-part-1"
tags = ["travel", "food", "Japan"]
title = "Japan Trip - Part 1"

+++


After we arrived at the airport I opted to purchase tickets for the coach bus into Tokyo that drops us off right in front of our hotel, the tickets cost ¥3000($30 USD) per person. The reason I chose to take the coach bus even though Tokyo traffic was known to be bad(took 2 hours to get to our hotel with traffic) was that while researching for the trip I figured we'd arrive late and tired and getting lost while transfering trains and looking for our hotel carrying luggage wouldn't be fun. This being the first time I'm going to Japan without a tour guide I was a bit nervous so I decided to play it safe. 

It was only at the end of our trip when we were going back to the airport that I found out the Narita express train was really close to our hotel in Shinjuku. Our hotel was walking distance to Shinjuku station which is one of the stops that the Narita express stops at so no transfering was necessary. I also found out during my train ride back to the airport that with a foreign passport you can get a [package deal](https://www.jreast.co.jp/e/pass/nex_round.html) for round trip Narita express ticket and a Suica card (pre paid value card you can swipe at train gates) which makes it much cheaper and easier than taking the coach bus. Throughout our trip we spent a lot of time figuring out what the fare would be, scavanging for change and lining up to buy tickets. With the Suica Card it would be just like a Metrocard in New York City or the Octopus card in Hong Kong. Lesson learned for our next trip.

![Line up for bus](images/IMG_20140103_155515.jpg)
There were lines and stations for multiple coach buses that depart towards different hotels. The attendant was very adamant about us lining up properly and staying within the lines drawn on the floor for our departure time.
![Smoking box](images/IMG_20140103_155510.jpg)
Smoking box right next to the coach lines. One thing I thought that made a lot more sense in Japan is that they make people smoke inside instead of outside like they do in the U.S. Makes more sense to keep the smoke in one confined area than to force people in public to smell / breathe it in. Smoking is something that you do inside in Japan or at designated smoking areas, you won't really find anyone walking while smoking a cigarette.

![Hotel Sunroute Plaza Shinjuku](images/1523641_10153700621780293_428938343_o-1-.jpg)
![Hotel](images/135254_10153700621875293_2112647772_o-1-.jpg)
Arriving at the Hotel

![Hotel Room](images/IMG_20140103_184736.jpg)
![Hotel Room2](images/1048507_10153700621095293_448576876_o-1-.jpg)
Here is the girl friend inspecting the bathroom of our small hotel room. We stayed at the [Hotel Sunroute Plaza Shinjuku](http://www.hotelsunrouteplazashinjuku.jp/en/). The room was quite small but every thing we needed was in there and it was clean and tidy.It was probably one of the best hotels I've stayed in my travels with a convinient location without being over priced.

![Shinjuku Bathroom](images/IMG_20140103_184709.jpg)
![Shinjuku Bathroom 2](images/IMG_20140103_184715.jpg)
Pictures of the bathroom. Bath tubs tend to be deeper to allow soaking compare to the U.S.

![Body Sponge](images/IMG_20140104_094332.jpg)
![Body Sponge rear](images/IMG_20140104_094337.jpg)

This was provided in the basket. It contained a threat that I had better use this sponge with body shampoo.

![Soap](images/IMG_20140104_093659.jpg)
But oh noes I don't have body Shampoo! Only body soap. I'll just have to mix the shampoo and body Soap together.

![Mirror](images/IMG_20140104_093650.jpg)
They put something on the mirror that prevents fogging after a hot steamy shower.

![Door signs](images/IMG_20140104_100923.jpg)
Signs that act like fridge magnets instead those annoying clumsy signs you hang on your door knob/handle.

![Shinjuku](images/IMG_20140103_204451.jpg)
First order of business was to go outside and search for a ramen place to eat
![GogoCurry](images/IMG_20140103_204846.jpg)
We walked past a [Gogo Curry](http://www.gogocurryusa-ny.com)
![Tokyo Streets](images/1524988_10153700621455293_1751708115_n-1-.jpg)
This was the street where the Ramen Place we went to was located. The place we went to was called [Santouka](http://www.santouka.co.jp/). I was surprised to find out that it's actually the same chain that they also have at the Mitsuwa Supermarket in Edgewater New Jersey that I frequently go to. The Ramen was obviously much better than the one in the states.
![Ramen 1](images/IMG_20140103_205702.jpg)
![Ramen 2](images/1048763_10153700621395293_1664789851_o-1-.jpg)
![Shinjuku lights](images/1499005_10153700621595293_1735825396_o-1-.jpg)
![More Shinjuku lights](images/1011765_10153700621695293_1101435630_n-1-.jpg)
The city is very alive even late at night. Saw many lights and people while walking back to the hotel.

![Batsu Game](images/IMG_20140103_190744.jpg)
Watching a bit of the brand new Batsu Game before bed! Batsu Game is a Japanese game show in which the participants aren't allowed to Laugh for 24 hours or they are punished. There is usually a theme associated with each series (Hospital, Police station, School etc). This year it was the [Earth Defense Force](http://en.wikipedia.org/wiki/Earth_Defense_Force_%28series%29). They broadcast it as a 6-8 hour special every year on New Years.


+++
author = "Anthony"
categories = ["opinion", "agile", "technology"]
date = 2015-04-25T02:16:58Z
description = ""
draft = false
slug = "what-do-agile-and-bruce-lee-have-in-common"
tags = ["opinion", "agile", "technology"]
title = "What do Agile and Bruce Lee have in common?"

+++


This post contains my personal opinion from my relatively short experience so far in the working world. I have no experience with project management or leading any teams.

## What is agile to you?
I was motivated to write this due to a recent conversation I had during a job interview. I asked the interviewer what the team structure was like and how did their team's workflow and Software Development Lifecycle work. This eventually led to him admitting that his team is trying to be agile but are essentially using a hybrid of scrum/waterfall model. I comforted him by saying that what they do is not far from what a lot of companies do, and I think it's called agilefall by most people, and if that model is working for their small team then that's great, but if they can improve the process by changing how they do things then they should definitely try changing things. I was then asked what agile meant to me. 

Being a Bruce Lee and martial arts fan I've noticed similiarties between Bruce Lee's martial arts philosophy and agile principles ever since agile was introduced to me at my first job out college 3 years ago, and I've used the ["be water my friend"](https://www.youtube.com/watch?v=VqHSbMR_udo) explanation on other's who's asked me the "what is agile" question.
>*“You must be shapeless, formless, like water. When you pour water in a cup, it becomes the cup. When you pour water in a bottle, it becomes the bottle. When you pour water in a teapot, it becomes the teapot. Water can drip and it can crash. Become like water my friend.” -Bruce Lee*

Other than the obvious agile concepts like short and quick iterations etc, agile to me means being like water. You need to be able to flexible and adapt to the problems your team encounter using what resources you have available. It also means constantly trying new things, learning from it, take what is useful and discard what isn't. Also keep in mind that what's useless now could be useful a year down the road or on another project you're working on. Bruce Lee studied many different styles of martial arts and incorporated it into his own.

Just like individuals, every team is different. I might an injury on my leg that doesn't allow me to use kick techniques as efficiently, your team might have a lack of senior developers due to budget restrictions. If something makes sense for your team due to whatever the situation is, then do it even if it means doing a waterfall model or going against agile principles. Gather metrics and do a comparison on whether it is truely better for your team and project. Dave Thomas, one of the authors of the [Agile Manifesto](http://agilemanifesto.org/) talks about it in his interview with Ruby Rogues [here.](http://devchat.tv/ruby-rogues/164-rr-staying-sharp-with-dave-thomas) He also talks about the bastardization of the term Agile. He wrote a great blog post about it here. [Agile is Dead(Long Live Agility)](http://pragdave.me/blog/2014/03/04/time-to-kill-agile/).

## Do you have Agile/SCRUM experience?
One of the things that automatically turn me away from job postings or recruiters is when they ask this dreaded question or "do you know SCRUM?". The agile principles and SCRUM methodology are very easy to understand and to follow. It doesn't take years of experience for you to understand it. What does matter is the type of people that you hire. Straight from the agile manifesto.
>Individuals and interactions over processes and tools

What you should be looking for is whether the person works well with others, can provide insight from his experiences and whether he would be the type of person who could balance agile's "just good enough" principle and seeking constant improvement in his work. A person can be a very experienced developer, worked in an agile environment for many years but not care about his work and not be motivated to learn/try new ways of doing things. I've encountered a lot of senior developers that has the "I've done it this way and it worked for me for many years so why change it" kind of attitude.

>*"The techniques, though they play an important role in the early stage, should not be too restrictive, complex or mechanical. If we cling to them, we will become bound by their limitation. Remember, you are expressing the technique, and not doing Technique number two, Stance three, Section four." - Bruce Lee*

A few projects I was on were obsessed with following SCRUM methodology even when it doesnt make sense. On one of them my team consists of 3 people. The project manager insisted that we held SCRUM meetings even though all 3 of us interacted with each other on a daily basis and know exactly what is going on with every thing. The meetings ended up being us repeating what we already know to each other.

On another project we held Sprint retrospectives that only included the dev team but not all stakeholders. A lot of the frustrations developers had with the way testers or product owners did things were shared with other developers already since we all had to deal with it, most of the problems and suggestions brought up were never heard by the other stakeholders. Sure they end up being summarized into a list and emailed off to them but I feel like this kind of bottlenecks a lot of potential discussions between the two teams that could have been brought up, especially when it comes to sharing solutions to similar problems that other teams encounter.

In the end, I believe it comes down to the individual. Are you hiring talented fast learners who are open minded, flexible, curious, pleasant to work with and put pride into the work they do? You can apply Agile principles and SCRUM to a team of people who don't possess any of those and I feel like all you'll be doing is doing very short iterations of what others have been doing before and labeling it with a buzzword.

<font size="2em">*Before creating this blog, I had a list I kept for blog topics on the "Out of Milk" shopping list app. this topic was one of the things that I had in mind. I was hestitating to make a post about it because I felt like I haven't been working for that long and I probably don't know what I'm talking about. So I decided to do a google search. At the time, when I did a google search there weren't any articles or blog posts about this comparison, but as of today when I decided to write this blog post and I did another search there are a few articles floating around now posted relatively recently. I guess the whole "your idea is probably not unique" concept that I've read about is very true. But I'm glad that there are others who noticed the same things as I did and my ideas aren't completely crazy.*</font>


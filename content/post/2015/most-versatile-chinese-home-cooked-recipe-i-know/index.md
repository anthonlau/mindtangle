+++
author = "Anthony"
categories = ["food", "recipe"]
date = 2015-08-01T12:50:23Z
description = ""
draft = false
slug = "most-versatile-chinese-home-cooked-recipe-i-know"
tags = ["food", "recipe"]
title = "Most versatile Chinese home cooked recipe I know"

+++


I approach most of my cooking with a very engineer type of approach. Look up a recipe, follow directions and use the exact amount listed on the recipe and use a timer and scale when necessary. This following recipe is one of the few recipes that I learned by watching others and doesn't follow  a particular formula and/or recipe (the others being mapo tofu and garlic mashed potatoes). 

I learned this recipe from my mom as a kid watching her cook. This recipe is more like a list of seasoning that you can use for all kinds of home cooked chinese dishes. This tends to bring out different kinds of flavor depending on what you're cooking, whether it be beef, chicken, pork, or shrimp. It's also very simple to make, the only way you can mess it up is probably adding too much salt or soy sauce. As with my other recipes of this kind, most of the portions for the ingredients and cooking time are learned through experimenting many times to find out the correct portion for your personal taste(I tend to use less salt than most people prefer). But this recipe in particular is so versatile that you might want to adjust the amount of seasoning you use based upon what kind of dish you're cooking.

So here are the basic ingredients for the marinade for the protein that you'll be cooking. The portions listed are going to be for the main example I've written below for shrimp with egg sauce.

**Basic Ingredients**

* 1/4 t Table Salt (kosher/sea salt if you're cooking steak)
* 1/4 t Sugar
* 2 T White pepper *
* 2 T Garlic Powder *
* 2.5 T Soy Sauce
* Coat with Cooking Oil with high enough smoke point for stir fry (Canola is what I use usually)
* A bit of Corn starch around one teaspoonful per 250g of meat to hold the solid seasoning together with the liquid.

	*\*these ingredients you can use liberally since it's very difficult to put too much in*

**Optional Ingredients**

* Grab a small bowl and mix some water with table spoon or more of cornstarch if you want to get a thick sauce. Make sure to mix it with your finger to get rid of any clumps that form.
* A bit of paprika if you want a little kick
* 1 T Shaoxing cooking wine for shrimp beef and chicken to give it aroma. You can replace Shaoxing with sake if it's not available in your area.
* Minced garlic for more intense flavor

## Shrimp with egg sauce
I'm going to use this dish as the main example since it's the easiest in my opinion and I made it so many times back in college due to how quick and easy it is since shrimp defrosts very fast. I tend to buy these huge bag of frozen shrimp from costco, put it in a bowl of water 2-3 hours before cooking to defrost and then squeeze the tail part of the shell off and it's ready for use.

![Shrimp](images/IMG_20141118_211433.jpg)

1. Season the shrimp with seasoning mentioned above start with dry ingredients before wet ingredients.
2. Heat up pan with medium high heat. Coat pan with Oil and wait until it begins to smoke a little, add in garlic and cook for about 30 seconds until you can smell it if you decided to add garlic
3. Cook Shrimp in pan. Beware of over cooking or it will become rubbery, these shrimps usually cook fast between 3-5 minutes.
4. Lower Heat to low heat and add in 3-4 lightly beaten eggs. Quickly mix it in with the shrimp until it's just right before the desired texture. Add in cornstarch water mixture if you want a thicker sauce. Remember that the egg will keep cooking after you plate it due to the heat. (Optional: Add in some chopped up green onions).

![Shrimp Ingredients](images/IMG_20150623_223018.jpg)
![Pre heat](images/IMG_20150623_224031.jpg)
![Cornstarch Water mix](images/IMG_20150623_223230.jpg)
![Shrimp Cooking](images/IMG_20150623_224150.jpg)
![Shrimp Cooking 2](images/IMG_20150623_224439.jpg)
![Shrimp Served](images/IMG_20150623_225243.jpg)

And that's it, that simple. I'll list some of the other variations that you can do with this recipe

## Sliced or Diced Mushrooms
Same recipe as above just use sliced / diced mushrooms and put it directly in the pan with the mushrooms instead of marinating it then mix it up before adding cornstarch water mixture.

## Oven baked marinated steak/pork/Salmon Steak
Ideally you want to season the meat for few hours. If I buy cheap cuts of steak I usually use this recipe to make up for the cheapness of the meat, usually flank steak. If I'm making it for myself I usually can get away with using a toaster oven making it easier to clean up and the toaster oven preheats much quicker. The juice from the meat from this recipe goes really well when served with rice.

**Marinade**

* Salt
* White pepper
* Sugar
* Garlic Powder
* Oil
* Soy Sauce

1. Preheat Oven to 500F or broil (425F for salmon steak)
2. Marinade the meat, poke some holes in it with a fork to allow the mariande to seep in and to tenderize it a bit.
3. Let it sit for a few hours if possible if not it's fine too.
4. For pork and beef, bake in oven for 8-12 minutes depending on how thick the piece of steak is and how done you want it to be. This particular flank steak shown was medium rare at 10 minutes. For Salmon steak, bake under 425 for 7 minutes then broil for 5 to caramelize the sugar a bit for browning.

![Pork](images/IMG_20141123_193503.jpg)
![Steak1](images/IMG_20150621_210710.jpg)
![Steak2](images/IMG_20150621_220405.jpg)

## Chicken/Beef with Brocolli/Bell peppers/Chinese bud chives/Whatever greens you want here

You want to go for chicken thighs for more flavor and it's less dry. For beef I usually get sirloin or skirt steak. Slice up the chicken/beef into thin slices to allow for faster cooking. Same Marinade as the shrimp with egg sauce recipe. Shaoxing wine will make a much bigger difference in this recipe.

The only other thing you want to do different in this recipe is 

* Might want to use a bigger/deeper pan or a wok 
* Start off cooking the Vegetable first until its halfway done. 
* Sprinkle some sugar on the greens to bring out the flavor. 
* Plate the greens temporarily then cook the meat until it's almost done (more rare if you want the beef to be medium rare) then add in the greens and the corn starch water mix and stir and mix until even and it's ready to be served.
* If you're using thicker/harder greens like brocolli and you prefer them to be softer instead of crunchy.  Towards the end when you mix it in with the meat, add more water into the cornstarch water mix, or just add more water directly, around 3 Tablespoons. Then cover the pan and steam it for a minute and it will quickly soften them.

![Chicken with brocolli 1](images/IMG_20141122_191635.jpg)
![Chicken with brocolli 2](images/IMG_20141122_192221.jpg)
![Chicken with Brocolli 3](images/IMG_20141122_192622.jpg)
![Chicken with Brocolli 4](images/IMG_20141122_192720.jpg)
![Cornstarch mix](images/IMG_20141122_192538.jpg)
![Chicken with Brocolli 5](images/IMG_20141122_193130.jpg)
![Bud chives](images/11188153_10100817816027085_3509589906006912396_n-1-.jpg)

## Try out your own recipe
These are the most common dishes I've made but I've heard of people doing other variations of it. Please feel free to share in the comment section how well this recipe worked out for you or if you tried something different with it.


+++
author = "Anthony"
categories = ["food", "austin", "restaurant", "review"]
date = 2015-09-21T22:01:32Z
description = ""
draft = false
slug = "not-so-good-austin-food"
tags = ["food", "austin", "restaurant", "review"]
title = "Not so good Austin food / coffee"

+++


I'm trying to avoid devoting a whole blog post to negativity but since I've been trying out new places to eat faster than I can blog. I think its much easier to just compile a list of places I didn't like with a short one to two sentence description of my thoughts on it. I seem to get remarks a lot about my bluntness, so this will also be good practice for me to choose my wording carefully.

![Chilantro1](images/IMG_20150709_205145.jpg)
![Chilantro2](images/IMG_20150709_205141.jpg)
**Chilantro**: Add some kimchi and bulgogi to a burger and fries and call it korean food. Loud mix of flavors that don't complement with each other well, and low quality meat used for bulgogi.

![Hightower](images/IMG_20150802_123549.jpg)
**Hightower**: Service was bad, pulled pork burger was cold and soggy. Loud rowdy atmosphere.

![CaffeMedici](images/IMG_20150816_125217.jpg)
**Caffe Medici**: Latte tasted like hot milk with no trace of coffee.

![Song La Truck](images/IMG_20150816_135807.jpg)
![SongLa2](images/IMG_20150816_144214.jpg)
![SongLa3](images/IMG_20150816_151535.jpg)
**Song-La Taiwanese Food truck**: Not that many people in line, but still waited 2 hours in 98F degree weather for food. Pork belly buns were okay, pork rice doesn't taste authentic, the egg was ice cold in the center and not seasoned fully. [(Pictures of what it's supposed to look like, can also easily be made at home)](https://www.google.com/search?q=taiwanese+pork+rice&espv=2&biw=982&bih=1632&source=lnms&tbm=isch&sa=X&ved=0CAYQ_AUoAWoVChMIxKTFhrGIyAIVBxSSCh1vvQC9&dpr=1.1)


+++
author = "Anthony"
date = 2015-04-16T23:44:53Z
description = ""
draft = false
slug = "hello-austin-why-i-risked-my-job-and-moved"
title = "Hello Austin. why I risked my job and moved"

+++


I was originally planning to move to Austin with the girlfriend back in August of 2014, but my manager convinced me to stay until our current contract ends and help him out doing my usual jack of all trade roles, also to train my replacement for my other project that I do deployments for once a month. I wasn't promised any remote work when I mentioned moving and might be forced to quit eventually. As of last week, I'm officially a resident of Austin after driving across Tennessee and Arkansas.

I have been talking about moving and potentially changing jobs for quite some time now since I've been asking for a more coding oriented roles for the past 2 years now at my current company. I kept getting promised a full time development role instead of having my time split amongst multiple projects doing different things. But every time when it seems like it's about to happen, politics come into play and other job managers from other projects makes me stay and support them part time. The dev work we did end up with are mostly identifying elusive bugs that are usually caused by our domain model, or keeping up with the governments last minute demands to upgrade Java and to satisfy the [HP Fortify](http://www8.hp.com/us/en/software-solutions/application-security/) scans all within a few weeks.

Most places I've interviewed with last year either said I was looking too far in advance and they need someone within the next few weeks, or that I don't have enough dev experience. That's when I realized if I wanted to dig myself out of the Systems Engineer pidgeon hole, I'd have to switch jobs or move to Austin so that I'm no longer able to support the deployments and other Systems Engineer activities. 

I really wanted to step away from the government defense contracting world since I feel like it's hindering my career development with all the red tape and politics within the military I have to deal with that takes up most of my time. The military is very resistant to change, but when change is mandated (Update Java for example) there is a knee jerk reaction instead of planning ahead and they want it completed in a short amount of time without understanding how software development works(Upgrading Java breaks code and you'd have to update deprecated methods and do regression testing). One of my coworkers explains this lack of understanding perfectly.
> *"If you gave the government a choice on how to hire people on our team they'd get rid of all the testers and replace them with developers and expect you to get things done twice as fast"*

In fact, this is exactly what happened towards the end of the contract when we were running out of money, we were downsized to two requirements analysts and four developers and zero testers. Everyone on the team was doing testing.

If I were to switch jobs I'd either have to start from a very junior level developer position or look for a DevOps position with a company that wouldn't mind that I came from a Windows oriented background with limited linux knowledge. I've had a few prospects right before I moved but one company decided that they wanted someone more familiar with linux after the 3rd interview, and most of the other opportunities didn't work out due to other candidates having more dev experience and side projects than me.

I have been spending most of my time after work reading up on things that helped with the Systems Engineering and deployment tasks I have. Many times I would have to find complicated work arounds for very simple problems with simple solutions due to the DoD network not allowing certain applications/tools being installed or used. I'd also have to research on topics I'm not familiar with as cipher suites when we were told to disable certain ones. All of this has taken away any time I have to do development projects on the side.

Now that I have moved, I was put onto the very project I have been helping with deployments for as a .NET developer until May. I am allowed to work remotely even though it wasn't promised to be before when I mentioned moving. I am most familiar with Java with almost zero knowledge of .NET and C# , but I was still given this opportunity. Most of my job managers know that I have been looking for jobs in Austin too. I feel like my job managers are confident in my ability to adapt and learn things fast on the fly, and that this is their way of saying thanks for dealing with the situations that have been handed down to me by them for the past couple of years. 

I have been hesitant to switch contracting companies for a much higher salary in the past mainly due to the great people I have been working with and the many mentors I have met. I believe the most difficult about finding a job is finding people that you love working with and that you can learn a great deal from, it's easy to find a higher paying job with more interesting work, but the people that you would be interacting daily with is very important too.

One of my job manager who is also my mentor has pushed me to look for a job in Austin. He told me that I needed to experience the commercial world and learn from it. He also told me to learn to say no. I have taken to heart the idea of being a sponge and absorbing as much experience as possible and accepting any sort of tasks and challenges thrown at me, but I need to learn to say no sometimes for my own good. In hindsight, I dug myself into this pidgeon hole precisely because of me saying yes to filling in the need for a Systems Engineer in the team. I love the Op's side of Software Development, but I believe my true love still lies within software engineering.

It's easy to stay in your comfort zone instead of taking risks and venturing into the unknown. I could've stayed in the DC area with my stable job or moved on to another contracting company for higher pay, but the only way for me to grow was to take the risk and leave the area which was filled with the same type of government jobs. I'm young, motivated, don't have a house or kids, I can afford to take these risks. Since I've moved here I've found it easier to land interviews and have even gotten a few offers, but I'll definitely be taking my time in choosing the right company. I'll also be using my free time now to work on some of the side projects I've had started.


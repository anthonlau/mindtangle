+++
author = "Anthony"
categories = ["travel", "food", "Japan", "Tokyo"]
date = 2015-04-01T13:27:16Z
description = ""
draft = false
slug = "japan-trip-part-4-last-day-in-tokyo"
tags = ["travel", "food", "Japan", "Tokyo"]
title = "Japan Trip Part 4 - Last few days in Japan"

+++


[Japan Trip Part 1](http://www.mindtangle.com/japan-trip-part-1/)<br/>
[Japan Trip Part 2](http://www.mindtangle.com/japan-trip-part-2/)<br/>
[Japan Trip Part 3](http://www.mindtangle.com/japan-trip-part-3-kyoto/)

# Leaving Kyoto
![Rauk](images/IMG_20140108_095644.jpg)
![Bread](images/IMG_20140108_100841.jpg)
![Melon pan](images/IMG_20140108_101001.jpg)

We started the morning off by visiting a small bakery around our Ryokan called [Rauk.](http://www.tripadvisor.com/Restaurant_Review-g298564-d1418918-Reviews-Boulangerie_Rauk-Kyoto_Kyoto_Prefecture_Kinki.html) If you're ever in the area I highly recommend you try out the bread here. I regret not coming here every morning. Biting into the bread felt like biting into a piece of cloud. Their bread is baked fresh every morning.

![Nishiki](images/IMG_20140108_105343.jpg)
![Market](images/IMG_20140108_105410.jpg)
![Market 2](images/IMG_20140108_110756.jpg)
![Aritsugu](images/IMG_20140108_110609.jpg)
Before leaving Kyoto we decided to do a last minute trip to the [Nishiki Market](http://www.japan-guide.com/e/e3931.html) since I wanted to purchase a knife from [Aritsugu](http://en.wikipedia.org/wiki/Aritsugu). A lot of famous sword making companies switched from making swords to making kitchen knives after the ban on swords was enacted in [1876](http://en.wikipedia.org/wiki/Hait%C5%8Drei_Edict). Since we got there early most of the stores were closed.

>*Nishiki Market (錦市場, Nishiki Ichiba) is a narrow, five block long shopping street lined by more than one hundred shops and restaurants. Known as "Kyoto's Kitchen", this lively retail market specializes in all things food related, like fresh seafood, produce, knives and cookware, and is a great place to find seasonal foods and Kyoto specialties, such as Japanese sweets, pickles, dried seafood and sushi.*

![Bullet train](images/IMG_20140108_122232.jpg)
![Bento Packaging](images/IMG_20140108_131534.jpg)
![Bento 1](images/IMG_20140108_131628.jpg)
![Bento 2](images/774176_10153700674375293_120165823_o-1-.jpg)
For this return trip back to Tokyo via the bullet train we remembered to stop by and get some bento boxes. They were really cheap. Each box was below $10 USD, they were nicely packaged and tasted amazing.

# Back in Tokyo
![Menu](images/IMG_20140108_193905.jpg)
![Menu2](images/IMG_20140108_194624.jpg)
![Yakiniku](images/IMG_20140108_195601.jpg)
![Yakiniku2](images/1559433_10153700674520293_1361027578_o-1-.jpg)
Back in Tokyo for dinner. My girlfriend insisted we try out a Yakiniku place in a basement right next to your hotel. The menu outside had a sign saying foreigners welcome! English menu available! It should've been a sign to stay away from the place. The food was okay, the quantity was small and terribly overpriced. Everything you see on the table here, few pieces of meat, rice and a bottle of sake came out to around $90 USD. After that expensive meal I was still hungry so we went to look for a ramen chain I'm familiar with. 

![Ichiran](images/IMG_20140108_211008.jpg)
![Preference](images/IMG_20140108_205159.jpg)
![Instructions](images/IMG_20140108_205205.jpg)
![Booths](images/IMG_20140108_205253.jpg)
![Tap](images/IMG_20140108_205318.jpg)
![Ramen](images/IMG_20140108_205603.jpg)
I've been to [Ichiran](http://www.ichiran.co.jp/english/) many times in my previous trips to Japan and their ramen never disappoints. You order with the same vending machine ticket system outside, then you sit down in small booths push a button to ring them and hand your ticket over to them under a curtain (you never see the ppl preparing your meal you only hear them and see them handing you the bowl. Along with your ticket you give them a piece of paper with preferences on how you want your noodle prepared. There's also a tap on the side for cold water.

# Last day in Tokyo
![Bookoff 1](images/IMG_20140109_120811.jpg)
![Bookoff 2](images/IMG_20140109_120944.jpg)
We stopped by a bookoff in Shinjuku and came across walls and walls of manga.

![Lunch](images/IMG_20140109_130825.jpg)
Had a caserole Lunch nearby

![Pokemon center](images/IMG_20140109_151220.jpg)
![Pokemon center2](images/IMG_20140109_144256.jpg)
![Pokemon center3](images/IMG_20140109_144301.jpg)
![Pokemon center4](images/IMG_20140109_151749.jpg)
We then went over to the pokemon center, where the girlfriend bought some cute toys and stationary, and I brought my 3DS and got a pokemon gift with a special hidden ability.

## Shibuya
![Hachiko statue](images/IMG_20140109_155555.jpg)
![109mens](images/1523299_10153700676600293_1350937961_o-1-.jpg)
![Shibuya crossing](images/1115946_10153700676870293_1180767262_o-1-.jpg)
![Crossing2](images/1512122_10153700677050293_476575585_o-1-.jpg)
We headed over to Shibuya afterwards and snapped a picture of the famous [Hachiko](http://en.wikipedia.org/wiki/Hachik%C5%8D) statue where a lot of people use as a designated meeting point. Then crossed the famous Shibuya crossing and explored a lot of famous shops. The Starbucks by the Shibuya crossing is the most profitable Starbucks in the world.

## Ikebukuro and Sunshine City
![Animate](images/1557390_10153700677670293_1754145858_o-1-.jpg)
![Anime2](images/5152_10153700677740293_847297841_n-1-.jpg)
![Sunshine city](images/IMG_20140109_183715.jpg)
We headed over to [Animate](http://en.wikipedia.org/wiki/Animate) in Ikebukuro which was a paradise for Manga and Anime lovers. They didn't allow pictures inside. Briefly checked out Sunshine City which seems to be an older mall that no one really goes to anymore, was pretty empty when we were inside.

## Sushi Dinner
![Sushi](images/IMG_20140109_220846.jpg)
![Sushi2](images/IMG_20140109_221834.jpg)
We asked the staff at the hotel to recommend a nearby Sushi place. My girlfriend hasn't really tried sushi before this trip. The chef was a great guy and chatted us up, asked us where we're from and how our trip was so far. I ordered an extra plate of the Sashimi and he gave us a few extra pieces of Toro for free. The whole dinner came out to around $80 USD. If I were to get the same amount of Sushi for similar quality in the U.S. It would've easily been over 100 USD.

## Leaving Japan
![NEX](images/IMG_20140110_060539.jpg)
Taking the Narita Express(NEX) train this time back to the airport.
![Vending Machine](images/IMG_20140110_061243.jpg)
Vending Machine Propoganda
![Inside of NEX](images/IMG_20140110_063254.jpg)
Inside of the NEX is roomy and clean.
![Saying bye](images/IMG_20140110_071825.jpg)
Sun Rising as we say goodbye.
![Last ramen](images/IMG_20140110_092851.jpg)
Last bowl of instant ramen at the airport.

I've learned a lot from this trip, even though I've been to Japan multiple times this was the first time I've been without a guide and planned every thing myself. There are still many places I want to go to and explore in Japan and I'll definitely be returning with more experience in planning under my belt.


+++
author = "Anthony"
categories = ["travel", "food", "Japan", "Kyoto", "Temples"]
date = 2015-04-01T11:37:17Z
description = ""
draft = false
slug = "japan-trip-part-3-kyoto"
tags = ["travel", "food", "Japan", "Kyoto", "Temples"]
title = "Japan Trip Part 3 - Kyoto"

+++


[Japan Trip Part 1](http://www.mindtangle.com/japan-trip-part-1/)<br/>
[Japan Trip Part 2](http://www.mindtangle.com/japan-trip-part-2/)

# Leaving Tokyo
Woke up in time for hotel breakfast. It wasn't particularly amazing but wasn't bad either.

![Natto](images/IMG_20140105_083410.jpg)
![Breakfast](images/IMG_20140105_084311.jpg)
Natto, fish, Miso soup and some fruits.

![Ticket](images/IMG_20140105_104036.jpg)
We went into the JR office asking the attendant there how to get tickets for Kyoto. He wasn't very good at english but managed to understand us and helped us buy all the tickets necessary to get to Kyoto and I was able to charge it to my credit card.

![Bullet train](images/IMG_20140105_130129.jpg)
![inside the bullet train](images/1497998_10153700627680293_518667792_o-1-.jpg)
![Legroom](images/IMG_20140105_104132.jpg)
![Legroom2](images/IMG_20140105_104421.jpg)
Boarding the bullet train, look at all that leg room! For those who don't know I'm 6'4" (195cm) tall. I wish I had half the amount of this legroom on planes.

![Tea plantations](images/IMG_20140105_124534.jpg)
It was a very scenic ride and we came across a lot of green tea plantations.

# Arriving in Kyoto
![Bento](images/IMG_20140105_130356.jpg)
Came by a store that sells train bento boxes after we arrived, we were tight on time and almost missed our train so we didn't get any bento boxes for the train to Kyoto.

![Kyoto Station Stairs](images/1544375_10153700627840293_1128624457_n-1-.jpg)
![Looking down stairs](images/1540455_10153700627900293_969253044_o-1-.jpg)
Arrived at Kyoto station, here's a set of stairs that changes colors. The outdoor stairs run parallel to all the stores and restaurants indoors.

![Kyoto](images/IMG_20140105_131829.jpg)
Here's me walking around the Kyoto neighborhood towards our Ryokan. We decided to check in before looking for food.

![Capsule Ryokan](images/IMG_20140105_132258.jpg)
![Room](images/IMG_20140105_133002.jpg)
![Capsule](images/IMG_20140105_133010.jpg)
![Sink](images/IMG_20140105_133019.jpg)
![Toilet1](images/IMG_20140105_133107.jpg)
![Toilet2](images/IMG_20140105_142921.jpg)
We stayed at a ryokan called [Capsule Ryokan](http://www.capsule-ryokan-kyoto.com/). It offered hostel like rooms, capsule rooms and private rooms. We ended up picking a private room for around 80 USD a night. The room was small but had everything we need. The sink and the Shower capsule was right in front of the tatami mats which opens up as storage. The toilet seat cover opens up automatically when you open the bathroom door. You can also wash your hands above the water tank so that grey water can be reused to flush.

![Ryokan guide](images/IMG_20140105_143349.jpg)
![Ryokan bus](images/IMG_20140105_143355.jpg)
The ryokan included a guide on what restaurants are nearby, activities to do. A map of the area and a guide on how to take a bus. In Kyoto at least, you're supposed to enter the bus from the rear and pay in the front as you exit.


![Ramen road](images/1557130_10153700628100293_1705938684_o-1-.jpg)
![Ramen road 2](images/1524300_10153700628180293_438650132_o-1-.jpg)
![Ramen Road 3](images/1518727_10153700628240293_1917135342_o-1-.jpg)
We headed back to Kyoto station and went to the 10th floor which is a whole floor dedicated to Ramen shops. There are ramen shops from all over Japan with different styles and flavors.

![Ramen machine](images/IMG_20140105_151811.jpg)
![Ticket](images/IMG_20140105_152148.jpg)
![Ramen](images/IMG_20140105_153222.jpg)
Most ramen shops have you order outside via a ticket vending machine. You simply insert money and press the buttons on the ramen and other sides and appetizers you want and it will spit out a ticket. You then line up and wait for a seat, when you are about to be seated you hand the ticket to the waitress and they will bring out your food by the time you have sat down.

![Food](images/IMG_20140105_155249.jpg)
![Food2](images/IMG_20140105_155404.jpg)
There was so many different restaurants and cuisines inside Kyoto station that it would take weeks to try all of them.

![View of kyoto](images/705084_10153700629015293_753272041_o-1-.jpg)
![Department](images/1490817_10153700629405293_669900787_o-1-.jpg)
View of kyoto from ontop of the station, and the department store inside.

![Kyoto streets](images/1487688_10153700629740293_1000217276_o-1-.jpg)
![Kyoto streets2](images/1488663_10153700629840293_1194013768_n-1-.jpg)
![Hongwangji](images/478693_10153700630125293_641653057_o-1-.jpg)
We did some exploring and headed towards [Hongwanji Temple](http://www.hongwanji.or.jp/)

![Temple2](images/1524665_10153700630520293_1451255996_n-1-.jpg)
![Temple3](images/1498073_10153700631100293_607444574_o-1-.jpg)

![Temple4](images/1015314_10153700632055293_1134131282_o-1-.jpg)
These guys were hitting that piece of log repeatedly for few hundred times.

![Leet parking](images/IMG_20140105_165104.jpg)
Look at this crazy parking skills

![bottles](images/1483975_10153700633060293_1905264306_o-1-.jpg)
Some Japanese believe that leaving bottles filled with water outside will scare away cats cause they are afraid of light reflecting off of them.

![Kyoto tower](images/1512556_10153700633395293_1226978929_n-1-.jpg)
Picture of Kyoto Tower. We took a train towards the Gion district for the night hoping to run into a Geisha. [Gion](http://www.japan-guide.com/e/e3902.html) is a very historical district known for their old buildings and Geishas.
![Gion station](images/1529784_10153700634550293_1060756407_o-1-.jpg)
![Gion](images/1401200_10153700633885293_1325959006_o-1-.jpg)
![Gion2](images/1493505_10153700634060293_695055817_o-1-.jpg)
![Gion3](images/1496084_10153700634135293_1727727157_o-1-.jpg)
![Gion4](images/1560643_10153700634445293_1972882166_n-1-.jpg)
![Teramachi](images/1511884_10153700634955293_625854426_o-1-.jpg)
![Termachi street](images/1609560_10153700635035293_1883362362_n-1-.jpg)
We didn't see any Geishas, but we enjoyed the night stroll. It was an amazing experience and it felt like we traveled back in time. Before heading back walked by a covered shopping arcade, it was late so most of the stores were closing. These kinds of covered shopping arcades are all over Japan.

![Ricebowl](images/IMG_20140105_215104.jpg)
![Ricebowl2](images/IMG_20140105_215100.jpg)
[Gyudon](http://en.wikipedia.org/wiki/Gy%C5%ABdon) and [Oyakodon](http://en.wikipedia.org/wiki/Oyakodon) for dinner at a small chain shop that sells rice bowls called [Nakau](http://www.eatingoutintokyo.com/Lunch/japanese-restaurant/japanese-restaurants-in-tokyo-nakau.html). it was around ¥500 (5 USD) for each bowl. We headed back to the Ryokan afterwards.

# Kyoto Day 2 - Fushimi Inari Shrine and Arashiyama
Our plans for today was to take the train into Fushimi Inari Shrine, then head to Arashiyama afterwards to look for a bamboo forest.

![Breakfast Suica](images/IMG_20140106_105026.jpg)
We stopped by another cafe at the train station for breakfast. Kyoto uses ICOCA Cards instead of Suica (Prepaid stored value cards for trains and buses). For this cafe inside Kyoto station it also takes the card as payment.

![Breakfast](images/IMG_20140106_105151.jpg)
![Kyoto station](images/902691_10153700635325293_934366445_o-1-.jpg)
Having breakfast and coffee while overlooking the busy Kyoto station.

![Inari Station](images/1531879_10153700635490293_582824863_o-1-.jpg)
Arriving at the Inari station. Inari looked like a very small town and the station was very small. You can see the huge amounts of people pouring out of the station blocking up traffic. Apparently it was peak season due to the new year, and exam time for students so many people come praying for good luck.

![Inari Gate](images/1040733_10153700635395293_1781544456_o-1-.jpg)
Entrance to the [Fushimi Inari Shrine](http://www.japan-guide.com/e/e3915.html). 
>*"Fushimi Inari Shrine (伏見稲荷大社, Fushimi Inari Taisha) is an important Shinto shrine in southern Kyoto. It is famous for its thousands of vermilion torii gates, which straddle a network of trails behind its main buildings. The trails lead into the wooded forest of the sacred Mount Inari, which stands at 233 meters and belongs to the shrine grounds."*

![Inari Shrine](images/1511539_10153700635980293_1728570882_o-1-.jpg)
![Fox shrine](images/1517861_10153700636040293_2147445528_o-1-.jpg)
Foxes are regarded as the messengers.
![Shrine 2](images/1522725_10153700636600293_773868214_o-1-.jpg)
![Wish](images/1008864_10153700637045293_1535882157_o-1-.jpg)
People bought these wooden plates, wrote their wish on it and hang it up.
![Floor](images/IMG_20140106_113623.jpg)
Look at how uniform the floor is.

![Map](images/IMG_20140106_114402.jpg)
Map of the shrine and the thousand [Torii](http://en.wikipedia.org/wiki/Torii) gates. We only managed to go halfway up before deciding to come back down so we don't use up the whole day.
![Gate](images/1504256_10153700638295293_797870020_o-1-.jpg)
Walking through the gates
![Rear gates](images/IMG_20140106_115914.jpg)
When you turn around you'll see the names of people and businesses that have donated each of these gates over many years. Seems like the higher up we went the older the gates were.
![Charms](images/IMG_20140106_115101.jpg)
We stopped by a little stand that sold charms for career, love, wealth, health etc. It went by an honor system so you would drop money into the box. I don't think the gods would bless theives with good luck anyways.
![Upper Shrine](images/IMG_20140106_120602.jpg)
![Upper Shrine 2](images/IMG_20140106_120856.jpg)
We got up to this part of the shrine where there was a rest stop for you to buy snacks and tea and decided to turn around.
![Exploring Inari](images/IMG_20140106_122147.jpg)
![Train station Inari](images/IMG_20140106_123331.jpg)
We did a little bit of exploring of the local town before taking the train towards Arashiyama
![Arriving at Arashiyama](images/IMG_20140106_132011.jpg)
Arrived at Arashiyama station. This was one of my favorite pictures for some reason, looks like it came straight out of an anime to me.
![Arashiyama map](images/IMG_20140106_132416.jpg)
![Arashiyama area](images/IMG_20140106_133309.jpg)
![People](images/1075329_10153700641270293_1860549474_o-1-.jpg)
![Geisha](images/IMG_20140106_133602.jpg)
![Geisha2](images/1609587_10153700641200293_1673256113_n-1-.jpg)
We got a bit lost and came across what seems to be another historical district. Then we ran into a Geisha! People started crowding her and asked her for photos so we felt kind of bad and decided to take a picture of her from afar.
![River](images/IMG_20140106_133953.jpg)
Reminds me a bit of Rurouni Kenshin movie.
![old train station](images/IMG_20140106_135821.jpg)
![Snacks](images/IMG_20140106_140809.jpg)
![Snack](images/IMG_20140106_140426.jpg)
There was an old train station in middle of nowhere. We went to a nearby store and bought some snacks.
![Bamboo path](images/1485935_10153700642400293_1545018395_o-1-.jpg)
After getting lost a bit more we found the bamboo path and walked through it. It was starting to get late so we decided to head back to Kyoto station for lunch.
![Melon soda](images/IMG_20140106_143956.jpg)

![Waiting room](images/IMG_20140106_150012.jpg)
This station had a heated waiting room to shield you from the cold.

![Porta](images/IMG_20140106_153017.jpg)
![Restaurants](images/IMG_20140106_153718.jpg)
![Restaurants2](images/IMG_20140106_154024.jpg)
![katsu](images/IMG_20140106_154431.jpg)
![Katsu meal](images/IMG_20140106_155900.jpg)
We discovered a Kyoto underground shopping arcade right next ot the Kyoto station. There was a dizzying amount of restaurant choices there but we settled upon a Pork Katsu specialty restaurant. I had a wild boar katsu which tasted a lot leaner and gamey. Afterwards we rested up at the hotel and went back out to the rice bowl place for dinner. We also went online and booked a guided bus tour for the next day for all the popular Kyoto shrines in the area since we found ourselves spending a lot of time taking the train and figuring out where to go.

# Kyoto Day 3 Temples
We went to many shrines and took many pictures throughout the tour. Towards the end of the tour we were starting to get Temple fatigue.
## Ryoanji Temple
![Ryoanji Temple](images/1150551_10153700643840293_1219038052_o-1-.jpg)
![Room](images/1412330_10153700644865293_169921270_o-1-.jpg)
![Zen garden](images/885386_10153700644760293_369998624_o-1-.jpg)
This was probably my favorite temple of the trip even though it was a bit small. The area was quiet and it had a zen rock garden inside. Apparently you can move around and sit at different spots but at every spot there will be one rock which you can't see. The only supposed way to see all the rocks was from above, representing god. As a human you'll always miss something or not understand something, and seeing things from different perspectives will allow you to view things that you might've missed.

## Rokuonji Temple (Golden Pavilion Temple)
![Sign](images/1075333_10153700649845293_267976081_o-1-.jpg)
![Golden temple](images/1511599_10153700650390293_1425426796_o-1-.jpg)
![Top of temple](images/1598564_10153700650670293_83528525_o-1-.jpg)
![Interior of temple](images/1521436_10153700652340293_847865128_n-1-.jpg)
This temple was plated with gold on the upper parts, and it waas previously burned down before in 1950.

## Kyoto Imperial Palace
![Palace entrance](images/1496452_10153700654890293_912602005_o-1-.jpg)
![Palace](images/1150443_10153700655675293_165938423_o-1-.jpg)
![Palace2](images/1531726_10153700655745293_1995982507_o-1-.jpg)
![Palace 3](images/1052201_10153700655900293_2130564830_o-1-.jpg)
![Shishinden](images/1000028_10153700658585293_591585367_n-1-.jpg)
![Shishinden2](images/1500803_10153700656640293_2076857701_o-1-.jpg)
![japanese cypress](images/1492470_10153700661250293_1228347076_o-1-.jpg)
This place was gigantic. They had us line up outside like school children, then we were escorted the entire time to make sure no one would sneak off and hide somewhere. They had different rooms and buildings for receiving different people and for different ceremonies.

## Lunch Sukiyaki at Mai
![Mai](images/IMG_20140107_131949.jpg)
![Chicken Sukiyaki](images/1596704_10153700662495293_1306015763_o-1-.jpg)
Stopped by a Sukiyaki restaurant for some Chicken Sukiyaki before heading towards Nara.

## Nara Park and Todai-ji Temple
![Deer](images/1025873_10153700663605293_156969808_o-1-.jpg)
![Deer2](images/1014551_10153700664095293_523554233_o-1-.jpg)
![Mascot](images/IMG_20140107_153036.jpg)
Nara is known for their deer. Their citys mascot is a deer. 
![Food please](images/1537648_10153700665190293_942939950_o-1-.jpg)
This deer followed our tour guide for a good 10 minutes.
![Nara temple](images/1522438_10153700666360293_16942986_o-1-.jpg)
![Todai-ji stats](images/1498980_10153700667005293_1459301581_o-1-.jpg)
![Buddha](images/1486135_10153700668425293_590038479_o-1-.jpg)
Apparently the largest seated wooden Buddha in the world.
![More deer](images/1403459_10153700670785293_1139926311_o-1-.jpg)
This deer has seen some shit.

## Back to Kyoto station
![Station stairs](images/1483837_10153700673655293_1610909337_o-1-.jpg)
We got dropped back off in Kyoto station and we got to see the stairs at night.
![Ramen Menu](images/IMG_20140107_182812.jpg)
![Ramen](images/1548054_10153700673840293_554218514_o-1-.jpg)
![Ramen2](images/IMG_20140107_183358.jpg)
![Vending Machine](images/IMG_20140107_191000.jpg)
![Cream of corn](images/IMG_20140107_191011.jpg)
Tried out Hokkaido style ramen on our last night in Kyoto. Outside the station I saw a hot can of cream of corn soup, decided to try it out expecting it to taste weird. But it tasted freakin amazing. Better than the canned soup you get from the supermarket.


+++
author = "Anthony"
categories = ["travel", "food", "Japan", "Tokyo", "Akihabara"]
date = 2015-03-31T11:21:43Z
description = ""
draft = false
slug = "japan-trip-part-2"
tags = ["travel", "food", "Japan", "Tokyo", "Akihabara"]
title = "Japan Trip Part 2 - Akihabara"

+++


[Japan Trip Part 1](http://www.mindtangle.com/japan-trip-part-1/)

We started the day off by going to the cafe across from the hotel and had some breakfast. Our plan today was to explore Akihabara.

![breakfast](images/IMG_20140104_103324.jpg)
All this cost around 8 USD.

![Bakery](images/1496160_10153700622250293_1715393502_o-1-.jpg)
We walked by this bakery right next to the Shinjuku station. Apparently its very famous in paris. We  came back on another day and tried it's pastries and bread out and they were delicious.

![Vending Machine](images/1498842_10153700622335293_1209012330_o-1-.jpg)
Typical vending machine inside the station. It vends both hot and cold drinks. One thing that helped a lot throughout this trip with the cold weather was buying a hot drink from these machines. Vending machines were everywhere, and next to almost every single one was a recycling bin. I found myself buying a hot drink and warming myself up by holding the can until I finish the drink, I'll come across another vending machine, throw the can away there and buy another hot drink.

![Train station 1](images/IMG_20140104_122938.jpg)
![Train station 2](images/IMG_20140104_124346.jpg)
Waiting for the train to Akihabara, notice how everyone lines up nicely between the lines. 
![Train station 3](images/1522832_10153700622380293_1029024264_o-1-.jpg)
The sign on the floor says women only train carriage during rush hour to prevent groping. It wasn't written in English unlike most of the other signs in the station. I wonder whether foreigners who don't understand the sign would get into trouble.

![AK48](images/1534857_10153700622520293_1874500608_o-1-.jpg)
After arriving in Akihabara the first thing we ran into was the AKB48 theatre, cafe and shop.
![Gundam](images/1077557_10153700622820293_926357452_o-1-.jpg)
![Gundam2](images/165942_10153700622705293_295766554_n-1-.jpg)
We then run into the gundam cafe with capsule machines outside of it.

![Akihabara Streets](images/883850_10153700622990293_1632385122_o-1-.jpg)
![Akihabara Streets2](images/1495952_10153700622910293_1472617590_o-1-.jpg)
We started walking around exploring looking for an game arcade. I felt like a little kid surrounded by toys and I couldn't decide where to stop or go.

![Arcade1](images/IMG_20140104_125916.jpg)
![Arcade2](images/IMG_20140104_125947.jpg)
![Arcade3](images/IMG_20140104_130403.jpg)
![Arcade4](images/IMG_20140104_130822.jpg)
![Arcade5](images/IMG_20140104_130854.jpg)
![Arcade 6](images/1501465_10153700624455293_504596111_o-1-.jpg)
![Project Diva](images/jpg)
We stopped by a Teiko arcade, later on we found out there were multiple of them in Akihabara. First floor of the arcade was filled with UFO machines. The rest of the building were filled with all sorts of Arcade games. Snapped a picture of the girlfriend playing Project Diva.

![Sticky pic machine](images/1523154_10153700624545293_1672548404_o-1-.jpg)
Not sure why this was needed outside a sticky picture booth.

![Art](images/1064970_10153700625275293_1989994915_o-1-.jpg)
Some random street art

![Backstreets](images/1513324_10153700625490293_1088440343_n-1-.jpg)
Backstreets in Akihabara with a lot of computer , hardware and random stores.

![Girls](images/1528639_10153700625510293_197311279_n-1-.jpg)
Not sure what it's advertising, seems like some sort of cuddling service or companion service. 2500 yen for 30 minutes and 3500 yen for 30 minutes.

![Toystore](images/IMG_20140104_132225.jpg)
![Toystore2](images/IMG_20140104_132256.jpg)
![Toystore3](images/1501548_10153700624270293_1198761020_o-1-.jpg)
We walked into a toystore with a lot of toys that I've been wanting since I was a kid, many of them are still on my amazon wishlist. I packed really light for this trip and wouldn't have had room to ship it back. Most stores in Akihabara took cash only, I didn't want to blow through all my cash this early on in the trip.

![Retro Game store](images/1548039_10153700625970293_279251461_o-1-.jpg)
![Retro Game 2](images/1506559_10153700626075293_1293315675_n-1-.jpg)
![Retro Game 3](images/IMG_20140104_144333.jpg)
![Retro game 4](images/IMG_20140104_144453.jpg)
![Retro Game 5](images/1500911_10153700626260293_796724910_o-1-.jpg)
We stopped by a few stores that had walls of retro games and consoles. They were dirt cheap compared to buying them in the U.S.

![Akihabara Map](images/1502625_10153700626860293_959408237_o-1--1.jpg)
At this point we were starving from walking around so much. We found a directory for the Akihabara area with all the stores and restaurants. This picture of the map was just part of the entire map. There was no way we could've explored the entire area in one or even two days.

![Curry place](images/1537768_10153700627210293_1576881380_o-1-.jpg)
![Curry](images/IMG_20140104_151848.jpg)
![Curry2](images/1487882_10153700627310293_796159429_o-1-.jpg)
We decided to stop by a Curry restaurant. The curry was amazing, but it was really spicy for japanese curry.

![Conbini](images/IMG_20140104_164806.jpg)
![Vending machines](images/IMG_20140104_165055.jpg)
At this point it was getting late so we took the train back, stopped by the conbini to pick up some snacks and drinks and went back to the hotel to get an early nights rest. We are going to leave early in the morning to head for Kyoto.


+++
author = "Anthony"
categories = ["travel"]
date = 2015-10-23T08:59:35Z
description = ""
draft = false
slug = "something-i-found-while-researching-plane-tickets"
tags = ["travel"]
title = "Price differences I discovered while researching plane tickets"

+++


I have been doing research/planning for the past few months for my trip to Japan and Korea next spring. The plan is to fly from Austin and land in Tokyo travel across to Osaka by bullet train and stop at places in between, then fly to Jeju Island, then to Seoul and back to Austin. Booking round trip is easy, but whenever you plan to visit multiple cities and countries, finding the best price and the best way to spend your credit card reward points can get rather tedious.  

Seasoned travelers might know about potentially lower prices for [Open Jaw flights](https://en.wikipedia.org/wiki/Open-jaw_ticket). I'm a fan of using [Kayak](www.kayak.com) in incognito mode as one of my many sources for researching ticket prices. Kayak will occasionally show "hacker fares" in the results which is supposed to be fares that are cheaper by combining multiple airlines and separate non round trip tickets. *(Side Note: booking directly from airline websites will guarantee you the cheapest prices almost all the time with some exceptions, but using 3rd party sites is more convenient)* . I used the multi city search function and it yielded this result for the two lowest priced tickets, notice the multiple airlines and hacker fare label. All prices are per person.

![Results1](images/eww.PNG)

Here are my results when I searched for an open jaw flight from Austin to Tokyo, then from Seoul back to Austin. And a separate search for the flight from Osaka to Jeju Island and from Jeju to Seoul. They are the exact same travel dates for 2 Adults.
![Results2](images/wut.PNG)
![Results3](images/corea.PNG)

Add the two lowest results together and you get $1674. That's a $1642 difference per person! For those who know me well, they know that I refuse to fly any non Cathay Chinese airlines, and that I would try to avoid any US airlines for long haul flights. Here are two more results for my preferred airlines for the US to Japan leg of the trip, JAL and ANA.
![Results4](images/ana.PNG)
![Results5](images/jal.PNG)
For simplicity sake lets just round up and say it will cost $2000 dollars if I choose a Japanese airline. Add the Korea flights in and that's still a $840 dollar difference from the Hacker Fare and I don't have to risk being tortured by China Airlines again, you can upgrade to Premium Economy Seats for two people with that money.

Just a reminder for people to do your homework and spend some time researching if they can afford the time and plan early.


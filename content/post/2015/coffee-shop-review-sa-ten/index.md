+++
author = "Anthony"
categories = ["food", "austin", "review", "coffee"]
date = 2015-08-28T11:09:41Z
description = ""
draft = false
slug = "coffee-shop-review-sa-ten"
tags = ["food", "austin", "review", "coffee"]
title = "Coffee Shop Review: Sa-Ten"

+++


I felt like I've been writing about food a lot recently, and was planning on taking a break by writing about either technical / personal development or traveling. But I've been so excited about Sa-Ten that I just could not wait to share this place with others. There isn't any other word I would use to describe this place besides ***Amazing***. This place was opened by Kome's Kayo Asazu and East Side King's Moto Utsunomiya. **If you have not been here yet and you're in Austin, definitely pay a visit!**

![Sa-Ten](images/IMG_20150726_121552.jpg)

## The Coffee
Located hidden inside what seems to be an old converted Warehouse with a docking area in East Austin, Sa-Ten is surrounded by a lot of art galleries and a jewelry store. For our first visit there we noticed someone working on her art with spray paint outside. We walked into a well decorated decently sized coffee shop. My girlfriend ordered a cold brew coffee and I ordered a Latte, they were both good and exceeded my standards. I've also ordered an Americano to go once and was pleasantly surprised by the flavor of the coffee even after being watered down. But it wasn't until my second visit where I tried their house special named Ohayo (Good morning in Japanese), where I was absolutely blown away. It's a cappuccino with brown sugar added into it. For those who know me, I usually dislike sweet flavors and I don't even add sugar into my coffee at home. The brown sugar in the Ohayo though was not overwhelming and balances out with the bitterness of the coffee, and enhances the coffee flavor immensely. This is definitely a must try for your first visit. Another thing that I have found impressive is that the flavor of the coffee has been very consistent. This has become my favorite coffee shop in Austin and I've been here multiple times, and every time (even with a different barista) the flavor has been similar.

![Latte](images/IMG_20150726_115709.jpg)
Latte
![Ohayo](images/IMG_20150809_145149-1.jpg)
Ohayo


## The Food
My first visit here was for coffee and a light brunch with my girlfriend. We came here not expecting much in terms of food, but the food was surprisingly just as amazing as the coffee. The best way I can describe the food is that this is what I personally imagine fusion should be like. You will often hear me complain about places serving "Americanized" or fake fusion food. Many places would add soy sauce or ginger to dishes to give it a hint of Asian flavor, call it fusion and charge you a premium for it. Sa-Ten calls themselves a Japanese-inspired coffee-house, and they live up to that claim. You can tell by the flavor of the food that it's still Japanese at it's core but its clearly served in a Western manner with westernized dishes such as toasts and sandwiches. The ingredients of dishes also provide a well balance of flavors that compliment each other in harmony, instead of having many "loud" flavors that explode in your mouth fighting over each other for your attention (though I do know some people that like this kind of flavor).

![Cold Chicken Katsu Sandwich](images/IMG_20150726_120335.jpg)
Cold Chicken Katsu Sandwich
![Sriracha Mayo Smoked Salmon Toast](images/IMG_20150806_125907.jpg)
Sriracha Mayo Smoked Salmon Toast
![Chicken Katsu Lunch Plate](images/IMG_20150809_145312.jpg)
Chicken Katsu Lunch Plate with Rice, Potato Salad and Kale Salad
![Chicken Katsu Lunch Plate 2](images/IMG_20150822_132906.jpg)
Chicken Katsu Lunch Plate with Kale Salad, Toast and Curry
![Teriyaki Curry Chicken Toast](images/IMG_20150809_145315.jpg)
Teriyaki Curry Chicken Toast

**Sa-Ten Coffee**

Hours:
Mon-Fri: 7am-10pm
Sat: 8am-10pm
Sun: 8am-8pm

In Canopy
916 Springdale, Bldg 3, Ste 101
Austin, TX 78702
map
512-524-1544
http://www.sa-ten.com/


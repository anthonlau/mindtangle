+++
author = "Anthony"
categories = ["travel", "bags"]
date = 2016-09-18T09:10:14Z
description = ""
draft = false
slug = "lessons-learned-from-packing-for-asia-trip-2016"
tags = ["travel", "bags"]
title = "Lessons learned from packing for Asia Trip 2016"

+++


In one of my [previous posts](https://mindtangle.com/asia-trip-2016-attempt-to-pack-light/) I showcased my first attempt at packing for my Asia Trip at the beginning of the year. I then followed up with my [review](https://mindtangle.com/bag-review-alchemy-equipment-carry-on-duffel/) of one of the bags I took with me and it showed what I ended up bringing with me.
![Final list](images/20160320_182334.jpg)
A quick list of items.

* Zanerobe sweat pants
* Outlier Slim Dungarees Pants
* Muji Cashmere Scarf
* Ice Breaker Merino Vneck T-Shirt
* Outlier Merino T-Shirt
* Mission Workshop Merino T-Shirt
* 1 White uniqlo OCBD
* 2 cotton T-shirts
* 2 pairs of ankle length Merino socks
* 1 pair of calf length Merino socks
* 3 pairs of exofficio boxer briefs
* 1 pair of Uniqlo airism boxer brief
* Nintendo 3DS
* Pikachu coin purse
* Electric Toothbrush with case
* 15" Macbook pro with charger
* Japan travel guide book
* Bose qc20 noise cancelling earbuds
* Moleskin notebook
* Nexus 7 for reading ebooks
* Three silicone travel size squeeze tubes (not shown in picture above)
* Bunch of cables and external battery (not shown in picture above)

# Lessons learned
* I'll get the most obvious thing out of the way. The 15" Macbook pro. Bringing it along was a huge mistake and a became a burden for most of my trip. If you read my review on the bag, putting it in the laptop slot in the bag prevented me from filling the main compartment to maximum capacity. This was the main reason why I ended up bringing a 2nd bag, the other reason was that I wanted to check in some more liquids and my safety razor. I brought it cause I was sure that I would blog every night, which would prevent myself from coming back home and not blogging about my trip for a year like my previous trip. I ended up getting back to my room very late and tired and not blogging even once. The most I used the Macbook for was to look up restaurants and google map our destinations for the day. Other than being big, I was also constantly worrying that I would snap the laptop in half in my bag or dropping my bag and breaking it. In the future if I really wanted to blog as I travel I think a bluetooth keyboard paired up with my Nexus 7 tablet would've suffice.
* I briefly mentioned it in my first point, but I brought a 2nd bag to be my carry on while I checked my safety razor, shaving cream and liquids in my first bag. Bringing a 2nd bag was a hassle and kind of defeated the one bag philosophy I was going for in the first place. I could've definitely got by with just purchasing shaving cream and a disposable razor at my destination.

![tubes](images/20160318_002721.jpg)

* In the picture above you can see the three silicone bottles I brought. One had body wash, one had face wash and the last one had moisturizer. I don't know why I didn't think of this when I packed but there was no way I could've used all of that on a two week trip. The body wash aside (I like the scent of mine and I know I'm not allergic to it), the face wash and moisturizer could have been in a smaller container such as [these](https://www.muji.eu/pages/online.asp?Sec=18&Sub=78&PID=1742) from Muji. The bottles I brought were still more than half full when I got home.
* Both my girlfriend and I brought our own electric toothbrush. The case we each brought for it could hold up to two additional tooth brush heads. We could've just brought one electric tooth brush in one case with 2 separate heads.
* Merino shirts are way too under rated. They dry fast when hung up in the hotel room and I could wear it few times with no smell developing. The cotton shirts I brought were just taking up extra space. In the future I probably won't bring any cotton T-shirts anymore.
* The exofficio and uniqlo airism boxer briefs I bought were both quick dry. I washed my boxer briefs almost every night so I could've definitely brought 2 or 3 boxers instead of bringing 4.
* Packing using the rolling method and packing cubes is definitely the way to go.
* For the colder spring months packing a warm but compact jacket such as the Uniqlo ultra light down jacket is a good idea. I found myself cold during some nights and the layers I brought were simply not enough.
* Noise canceling earbuds were a life saver on the plane full of crying children. It didn't block out the noise as well as the over the ear versions QC25, but I prefer the space I saved by bringing the earbud version instead. It only really had problems blocking the high pitched screams.

Over all I'm pretty happy with what I packed and I'm looking forward to my next big trip where I can apply the lessons I learned.


+++
author = "Anthony"
categories = ["review", "shirts", "outlier", "mission workshop", "icebreaker"]
date = 2016-06-06T06:34:02Z
description = ""
draft = false
slug = "shirt-review-merino-shirt-showdown-2"
tags = ["review", "shirts", "outlier", "mission workshop", "icebreaker"]
title = "Shirt Review: Merino Shirt Showdown"

+++


Most travel blogs swear by merino wool shirts. They are supposed to be odor resistant,keeps you dry, and works in both warm and cool temperatures. You can find plenty of information out there regarding the pros and cons of merino wool shirts so I'll cut to the chase. The most popular brands out there for merino shirts seem to be [Icebreaker](http://www.icebreaker.com/), [Outlier](http://www.outlier.cc/) and [Mission Workshop](http://missionworkshop.com/). I bought one of each shirt before my trip to Asia. Recently I came across a post on the Outlier subreddit asking if anyone owns the mission workshop and outlier shirt and how they compare. I decided to write a quick comparison.

# Stats and Shirt Info
**Height:** 6'4" / 193cm  
**Weight:** 205 lbs  
**Waist:** 33" / 83.82cm  
**Chest:** 40" / 101.6cm  
**Shoulder:** 22" / 55.88cm

Being an endomorph my weight fluctuates quite a bit. During my trip with these shirts I weighed 195 lbs and I've since bulked up a bit and gained another 10 lbs. I weight 205 at the time these pictures were taken.

All shirts are size Large
**Outlier:** $98 / 45 Day return policy Free return shipping / No Warranty  
**Mission Workshop:** $72 / ?? day Return Policy free return shipping / Life time Warranty on defects  
**Icebreaker:** $70 (I got mine for $42 on sale) / 365 Day Free shipping return policy unwashed and unworn in original packaging / 1 year Warranty on defects

# [Outlier ULTRAFINE MERINO T-SHIRT](http://shop.outlier.cc/shop/retail/ultrafine-merino-tee.html)
![Outlier Shirt](images/20160605_150358.jpg)
Outlier has been getting a lot of attention in the recent years. Their slim dungaree pants seemed to have made shock waves in the community and is constantly out of stock in most sizes. Being a tall guy, I ended up waiting over a year for the long sizes in the color I wanted to restock. I won't write a review about their pants since they are very popular and you can find them all over the internet. Their merino shirt is just as popular and I can see why. The shirt is soft to the touch and probably the softest out of the three, it's also pretty thick so I don't see holes forming easily in it from friction.
## Outlier Fit
![Outlier Fit](images/outlier2-1.jpg)
Their fit is a regular crew neck T-shirt fit. It's not baggy on me by any means but it's also kind of loose around the waist which isn't a huge deal to me since I have broad shoulders and slim waist and many shirts out there fit like this on me. I would say the fit is similar to Hanes premium T-Shirts you can get anywhere. 
![Outlier length](images/20160605_150426.jpg)
![Outlier length2](images/outlier1.jpg)
The only exception is the length. Outlier was the longest of the three. Both Outlier and Mission Workshop advertise that their shirts are longer so they allow some shrinking when washed. But Mission Workshops shirt is actually the shortest of the three. The Outlier shirt after couple of washes fit me perfectly and I have no issues with exposing my belly button when raising my arm. Obligatory dirty mirror selfie picks.

# [Mission Workshop The Sector Merino Crew Shirt](http://missionworkshop.com/products/apparel/sector-merino-crew.php)
![Mission Workshop Shirt](images/20160605_150325.jpg)
I'm a huge fan of mission workshop's design philosophy and their bags. Mission Workshop was started by the same people who owned Chrome Industries and sold it. This shirt was soft but not as soft as Outlier, though the material feels a bit smoother and less closer to cotton compared to Outlier, might be due to the mix of nylon they added in for extra strength and durability.
## Mission Workshop Fit
![Mission Workshop Fit](images/mw1.jpg)
This was my favorite fit out of the three. It's slim yet not too tight. The sleeves are a bit shorter than Outlier's. My only complaint has to be the length. As I've said before it was the shortest out of the three and even raising one arm slightly would expose some skin, and reaching for something up high would definitely expose my belly button.
![Mission Workshop Fit2](images/mw2.jpg)
This would've easily been my favorite shirt out of the three if it was longer.

# [Icebreaker Anatomica Short Sleeve V](http://www.icebreaker.com/en/mens-underwear/anatomica-short-sleeve-v/103035.html)
![Icebreaker](images/20160605_150344.jpg)
Icebreaker was the lowest price out of the three. The material is super thin but also very comfortable. This was the only company that offered a V-neck option and different grades of merino varying in thickness and how much nylon was mixed into it. I've heard lots of complaint on reddit about holes forming really fast due to the how thin their products are, but so far it has been fine for me.
## Icebreaker Fit
![Icebreaker fit](images/ib1.jpg)
![Icebreaker comparison](images/20160605_150456.jpg)
The length was in between Outlier and Mission Workshop. Other than being thin, the material is a bit stretchier which gave it an overall very comfortable fit even though it seems a lot smaller than the Outlier shirt.
![Icebreaker arm](images/ib2.jpg)
Lifting my Arm was no problem too, barely covering my tummy.

# Additional Thoughts / Conclusions
* Mission Workshop had the best choice of colors in my opinion, while Outlier's choices were not very attractive to me. I found myself trying to figure out which one I hated the least without getting black.

* I would buy more shirts from Mission Workshop if they would make them much longer

* Outlier's always out of stock for my sizes, I had to wait quite a while.

* Mission workshops material and fit was my favorite, but I'll probably have to wait longer to see how long each shirt lasts.

* Icebreaker is probably the best bang for the buck for people on a budget and you'll find them regularly on sale unlike the other two options.

* I need to clean my mirror

Merino shirts are awesome, they performed just as advertised. I'm known to sweat easily when walking and it kept me dry even with a backpack against my back all day. I was able to wash them at the hotel room and they would dry in a few hours. The temperature varied quite a bit on my trip and the shirts kept me both warm and cool. 

If I had to buy more shirts, I'd probably go with Icebreakers for now simply due to the price and the fit, and the wide variety of choices in style. If Outlier would slim down their fits a bit more I would totally pay the premium price for it, and if Mission Workshop would lengthen their shirts I'd probably replace my whole wardrobe with their shirts. I like all three shirts and I suggest everyone to try them all out with the generous return and exchange policies to find out which ones the right fit for you.


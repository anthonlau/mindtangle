+++
author = "Anthony"
categories = ["travel", "food", "nyc", "new york", "chinatown"]
date = 2016-03-21T00:30:28Z
description = ""
draft = false
slug = "hk-style-cafe-in-ny-chinatown"
tags = ["travel", "food", "nyc", "new york", "chinatown"]
title = "iM Star cafe in NY Chinatown"

+++


I was meeting my aunt for lunch in NYC chinatown and she decided to bring us to a HK style cafe called [iM star](http://www.yelp.com/biz/m-star-cafe-new-york) (called M star on yelp for some reason). The HK style french toast was nowhere near as good as the ones you can get in Hong Kong but the other dishes were on point. They have a wide selection on their menu and foods for breakfast lunch and dinner.

![HK style sharkfin](images/20160319_141004-4.jpg)
HK style fake shark fin soup where shark fin is replaced with rice noodles.

![Toast](images/20160319_141007.jpg)
![Toast 2](images/20160319_142900.jpg)
Condensed Milk and Butter Toast

![French Toast](images/20160319_141250-1.jpg)
French Toast and HK style Milk Tea

![Rice noodle roll](images/20160319_142117.jpg)

This place isn't good for big groups but worth a visit for those looking for an authentic experience.


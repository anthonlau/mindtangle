+++
author = "Anthony"
categories = ["opinion", "showerthoughts"]
date = 2016-01-01T05:03:33Z
description = ""
draft = false
slug = "failure-when-to-give-up-and-why-we-should-change-the-way-we-see-failure"
tags = ["opinion", "showerthoughts"]
title = "Why knowing your limits and when to give up is important."

+++


We've all heard these phrases 

> *"never give up"*

> *"you never know until you try"*

> *"if at first you fail, try again until you succeed"*

But what if you are able to realize that your goal is either impossible or will take a very long time to succeed before you completely fail or even better, before you even try? Wouldn't that lower your [opportunity costs](https://en.wikipedia.org/wiki/Opportunity_cost) and [sunk costs](https://en.wikipedia.org/wiki/Sunk_costs)?

I know the quotes above aren't meant to be taken literally, but I do know some people who would never give up, or bash their head against a problem for a very long time until they solve it. I have done that myself. I actually find that persistence to be a very admirable and valuable trait in life. On the other end of the spectrum you have people who will give up before even trying or whenever things go slightly wrong, which is obviously not the way to go either.

## Why is it important?
I've seen some of the new hires at work who are struggling with their task being too afraid to ask too many questions. They don't want to come across as being incapable by asking too many questions, but at the same time sometimes if they take too long to complete their task it could impact the deadline or other tasks that depend on it. Ideally you would want to try hard enough to learn something, and to make sure that you ask the right questions when you do ask. But how do you know when to give up and ask for help? There are many other examples you can apply this to outside of work, we've all seen plenty of DIY home improvement disasters on the internet. Which ends up costing people more than if they paid someone else to do it.

## Knowing your limits
I believe that knowing your limits is arguably a quality of a person that is just as important as persistence and learning from your mistakes. In fact, I think the ability to be persistent, knowing your limits and learning from your mistakes go hand in hand. I would argue that the ability to realize what and where your limit is and how to use that knowledge is more difficult to master than being persistent and learning from your mistakes. All three go hand in hand. Persistence helps you reach your limit, learning from your mistakes helps you raise your limit for the future, and knowing your limit will help you maximize the returns and save costs. The cost could range from your time, to money, to your health.

We all know that lifting weights tear up your muscles and you get stronger when they repair the muscle fibers. The heavier you lift the more progress you will make. But on the flip side if you force yourself to lift more than your body can handle you'll injure yourself and impede your progress. I love using weight lifting as an example because most people who have lifted weights will experience their body telling themselves *"stop! give up, this is really heavy, I can't do this"*. But with a little push most of the time you'll realize that you're actually able to do it, it just won't be easy. Most of the time it's just a mental obstacle. Once you bypass that mental obstacle you'll get a feeling of accomplishment and you'll be pushing your mind and body to it's limits and most likely see immense progress. Take caution though, cause for those who are able to ignore the mental obstacles can easily injure themselves too. We've all seen people who hurt their back from forcing themselves to lift something too heavy. These obstacles could be lack of skill, lack of strength, lack of self discipline (eg. procrastination), or even people around you who are bringing you down.

Anyone can keep trying and not give up, and learning from your mistakes is very obvious in hindsight for most cases. But actually finding out what your limit is, a physically non existent ceiling and realizing that you've hit it and when you've raised that ceiling is much more difficult.

## Discovering your limits
I personally feel discovering your limits is an on going progress which is a part of improving yourself endlessly throughout your life. You can find many resources on the topic of self-improvement so I'll avoid talking about that. I don't think one can truly know their limit, assuming you're constantly improving yourself, your limit should be always changing. I don't claim to know of a framework or a set of rules to follow that will help you discover your limit. I'm writing about this to hopefully raise awareness to others on the importance of this skill that is often forgotten about or undervalued. What I will do though is list some observations and lessons learned from my own personal experience.

* Weight lifting has helped me lift a lot of my mental barriers and raise my self confidence in my skills and abilities outside of the gym. Your body is your measuring stick and a sign of your progress.
* Learning something new every day, no matter how irrelevant it may seem now to what you're trying to achieve now it might be useful in the future or help you think outside the box.
* Having a mentor that will give you good advice and call you out on your bull shit, has helped me push my limit.
* Surround yourself with smart and great people. If feel like you're the smartest person in the room then you're in the wrong room. Learning from smarter people is a usually a more efficient compared to figuring stuff out yourself. It also doesn't hurt to have the option to do both. Which leads to...
* No matter how inexperienced or stupid someone seems, you'll still be able to learn something from them almost all the time.
* Work hard on improving yourself, but also make sure you get adequate rest. Constantly injuring yourself or constant mental fatigue will affect your limits and lower/stagnate them in the long run.
* Discovering the true difference between rest and procrastination is part of knowing your limits. You want to force yourself to work when you're procrastinating, and you want to force yourself to rest if you are forcing yourself to work through fatigue.
* You don't always have to wait until you reach your limit to give up, especially if the opportunity cost is too high.
* Listen to other people's advice especially if it's their expertise, but don't take their word for it without confirming yourself, and always look for ways to improve on things that they tell you.
* We all know this, but realize that your time left on this world is limited, and knowledge is arguably unlimited.

## Final thoughts
I'd like to conclude with talking about your true limit and diminishing returns. For most people it's very unlikely that you'll reach your true limit in which you can't improve upon anymore. But the skill ceiling and potential for some people will be much lower, whether it be lack of natural talent or a physical limit. Realizing when you can't improve yourself anymore in a specific skill anymore is important since it will allow you to dedicate more resources on improving something else. Similarly, finding out when you're just simply not improving as much anymore due to diminishing returns as you get closer to the skill cap is just as important. Since you can also when it's not worth it anymore to dedicate resources to that particular skill and reallocate it to something else.


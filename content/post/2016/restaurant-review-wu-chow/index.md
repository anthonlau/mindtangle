+++
author = "Anthony"
date = 2016-02-16T12:28:12Z
description = ""
draft = false
slug = "restaurant-review-wu-chow"
title = "Restaurant Review: Wu Chow (Dim Sum)"

+++


I was going to review my favorite restaurant in Austin (Daito), but felt like I should have a change of pace and write about a restaurant that isn't Japanese. For Valentine's day, I managed to make a reservation for morning Dim Sum at Wu Chow (although they open at 11 AM and by dim sum standards, that's afternoon and when all the leftovers are served). My girlfriend has been in love with dim sum since our trip to Hong Kong and has been excited about Wu Chow opening. Since I have lived in Hong Kong, New York and Los Angeles, I kept my expectations low for a place that served Dim Sum in a trendy manner.

![Chopsticks](images/20160214_111037_HDR.jpg)

The dim sum selection was larger than I expected even though it's much much smaller than an actual dim sum restaurant. We decided to order the basic staples of dim sum, [Har gow](https://en.wikipedia.org/wiki/Har_gow), [Siu mai](https://en.wikipedia.org/wiki/Shumai) and [Cha siu bao](https://en.wikipedia.org/wiki/Cha_siu_bao). In addition we also ordered phoenix tail shrimp, scallion pancakes, turnip cake, soup dumplings and sesame fritters for dessert.

![Menu](images/o.jpg)

I walked into this place expecting the food to be absurdly over priced (dim sum should be cheap, in most places at least), in small portions, and also have the authentic flavors of the dishes modified to cater to those who are not familiar with dim sum. I was mostly wrong on all three counts.

I'll start with the two dishes that I was pleasantly surprised with, the scallion pancakes and the turnip cake. In my opinion these two were better than any dim sum place I have ever been to, Hong Kong and Guang Zhou included. Though to be fair, scallion pancakes aren't a typical dim sum dish that is served / specialized everywhere.

The scallion pancakes were cooked perfectly. Scallion pancakes are one of those things that have very simple ingredients and cooking instructions, but are very difficult to cook properly. A lot of places end up making them too thick or too thin, and in turn this causes it to over/under cook and it ends up either falling apart, being burnt, or being too chewy or too flaky. Another problem is controlling the seasoning. If it's not seasoned properly or spread evenly you'll either have something that tastes very doughy and plain, or too salty, or have hot spots of saltiness / blandness. The scallion pancakes in Wu Chow were perfectly balanced in terms of flavor and texture, a feat which is under appreciated by many people.

![Scallion Pancake](images/20160214_112016.jpg)

The turnip cake is another dish that was cooked amazingly well. This dish is very popular at a lot of dim sum restaurants and in many cases would be sold out early in the morning. Many turnip cakes out there are very mushy inside and fall apart very easily as soon as you try to pick it up with your chop sticks - like a slightly tougher version of tofu - but not at Wu Chow. Turnip cakes are another dish that is prone to being over cooked on the outsides due to how thick the cake is, and due to the difficulty in cooking it evenly while preventing a burnt crust from forming outside that is similar to charred steak. Wu Chow has a nice brown crust on the outside of their turnip cake. I have no idea how they did that but my guess would be that they steamed it either before or after searing the outsides to cook it evenly. In terms of flavor, turnip cake usually has an overdose of umami flavor that overwhelms the turnip flavor due to the overuse of MSG, I usually get insanely thirsty after having a few bites. Wu Chow seems to have brought out the turnip cakes' flavor and characteristics perfectly.

![Turnip Cake](images/20160214_112235.jpg)

The Cha Siu (filling) in the Cha Siu Bao was also done perfectly, which surprised me. However, the Bao itself could use some more work - it was overly moist on the outside and the inside was a bit too flaky.

![Cha Siu Bao](images/20160214_112016_HDR.jpg)

We ordered the phoenix tail shrimp expecting something with sauce similar to [Phoenix Talon (Chicken Feet)](https://en.wikipedia.org/wiki/Chicken_feet). It ended up being a deep fried breaded ball of shrimp. Not much to say about it. It tastes exactly what it sounds like. Usually a dish like this in a Chinese restaurant would be served with some mayo to complement the pedestrian flavor of deep fried foods.

![Phoenix tail shrimp](images/20160214_111815.jpg)

Both the Har Gow and Siu Mai suffer from the same problem - over sized chunks of shrimp. The size of each piece of har gow and siu mai was huge due to this, and much bigger than what I'm usually used to. The shrimp flavor was definitely overwhelming, and I almost couldn't taste the pork at all for the Siu Mai. You can see a whole shrimp in one of my Siu Mai, they are usually chopped up and mixed with the pork. Other than that, the skin on the Har Gow was neither too thin or too thick and both dishes were cooked very well.
![Har Gow](images/20160214_112122.jpg)
![Siu Mai](images/20160214_114624.jpg)

The soup dumplings barely had any soup inside them and the flavor was average, but good by Austin standards.

![Soup Dumplings](images/20160214_112937.jpg)

The Sesame Fritters were on par with what I had at other places, but seemed to be over filled a bit, which may be a good or bad thing depending on personal preference.

![Sesame Fritters](images/20160214_115536.jpg)

# Closing Thoughts

**The Good**

* Not as over priced as I thought - reasonably priced by downtown Austin standards
* There were some dishes that were amazing and better than most Chinese restaurants you find in cities like NYC
* The service was great, something that is often lacking in most dim sum places
* Flavors and choice of dishes were very inviting to people who are not familiar with dim sum
* Much quieter environment than your traditional Dim Sum Restaurant

**The Not So Good**

* Still overpriced by dim sum standards
* Opens at 11am which is very late for dim sum
* Small selection of dim sum

**The Bad**

* Difficult to make reservations according to some people
* Some dishes are definitely catered towards the western audience

So it turns out I was wrong about them being absurdly overpriced, and the portions definitely weren't small - but that wasn't always a good thing (the oversized shrimp were hard to eat). For those of you who have never had dim sum before, or are intimidated by some of the dishes that seem exotic to you, I would definitely recommend trying out Wu Chow to ease your transition. Even though Wu Chow's dim sum exceeded my initial expectations, I don't see myself coming back for dim sum again, but this experience definitely has me curious about the dishes they serve for dinner and I'll probably be coming back to try that. All the food we ordered above came out to around $50. If we ordered the same things at a dim sum restaurant in NYC it would be around $25. It was definitely nice to be able to make reservations and not have to wait in line outside in the NYC winter, and to eat in a more quiet atmosphere.

**Presentation:** 3/5

**Quality:** 5/5

**Taste:** 3/5

**Price:** Expect to spend ~$50 for two people

**Price to Quality Rating:** 4/5 if you're desperate for dim sum in downtown Austin, otherwise 2/5

Wu Chow
500 W. 5th
Austin, TX 78701
512-476-2469

Hours:
Lunch: MON-FRI 11am - 2pm (reservations)
Dinner: MON-THU 5pm to 10pm & FRI-SAT 5pm to 11pm
Dim Sum: SUN 11am to 3pm
BAR Open Late


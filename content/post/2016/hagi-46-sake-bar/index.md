+++
author = "Anthony"
date = 2016-03-22T07:11:34Z
description = ""
draft = false
slug = "hagi-46-sake-bar"
title = "Hagi Sake Bar 46"

+++


I've discovered my new favorite place to go to in NYC. I read about this place from another blogger I follow, [Eataku](http://www.eataku.com/post/138613833456/hagi-sake-bar-46-nyc). I've been dying for an izakaya to open up in Austin, [Fukumoto](http://mindtangle.com/restaurant-review-fukumoto/) ended up being a great restaurant but I left disappointed since it was more of a Japanese restaurant than an izakaya in terms of prices, types of food and atmosphere.

My first impression of hagi after seeing the menu was "this isn't fukumoto, this is a real izakaya". They have a vast selection of sake on the menu and also stuff on the shelf that isn't on the menu. There is also a large amount of food to choose from, anything from small appetizers to snack on to a bowl of ramen or rice. Everything also has a picture for reference. The staff gave me great recommendations when I asked for it, and all of them were very warm and friendly. We happened to strike up a conversation with someone sitting next to us at the bar and he gave us some recommendations for places to visit for our trip to Osaka.

![menu](images/20160319_211819-1.jpg)
![menu2](images/20160319_211821.jpg)
![menu3](images/20160319_211827.jpg)
![menu4](images/20160319_211830.jpg)
![menu5](images/20160319_211834.jpg)
![sake](images/20160319_211926.jpg)
![sake2](images/20160319_211928.jpg)
![sake3](images/20160319_212012.jpg)
![sake4](images/20160319_212015.jpg)
![sake5](images/20160319_215610.jpg)

I had zero complaints about all the dishes, in fact they were all amazing. I was skeptical about the pizza tempura but it tasted out surprisingly well.
![Pizza tempura](images/20160319_213310-1.jpg)

Age Dashi Tofu
![Age Dashi tofu](images/20160319_213022.jpg)

Okonomiyaki
![Okonomiyaki](images/20160319_213221.jpg)

Croquette
![Croquette](images/20160319_214338.jpg)

Miso Glazed Grilled Mackerel
![Miso Saba](images/20160319_221136.jpg)

If you live in NYC and you haven't tried this place yet then you should definitely pay them a visit. The price wasn't too bad too. We were full from all the food we ordered and I lost track of how much alcohol I ordered. It came out to be 90 dollars for two people. Note that there is one location on 49th street and another on 46th.


+++
author = "Anthony"
categories = ["Japan", "bags", "review"]
date = 2016-06-05T17:41:00Z
description = ""
draft = false
slug = "bag-review-alchemy-equipment-carry-on-duffel"
tags = ["Japan", "bags", "review"]
title = "Bag Review: Alchemy Equipment Carry-On Duffel"

+++


![Carry on](images/alchemy-carry-on-2-1-.jpg)
I was asked by a few people since I got back from Japan about the [Alchemy Equipment Carry On](http://www.alchemy-equipment.com/carry-on.html) bag I bought for my trip to Asia. My previous post had pictures and a small review of me putting stuff for my trip into it [here](http://mindtangle.com/asia-trip-2016-attempt-to-pack-light/). 

![Bags](images/20160320_184502.jpg)
Now that I've carried it around New York and Asia for two and a half weeks I feel like it deserves a more in depth review. First off, I ended up bringing an additional bag since I wasn't able to fit some stuff I wanted to carry on such as external battery and cables and my 15" Macbook pro. Also I wasn't able to carry on my double edged safety razor and liquids so I had to check the bag. I ended up buying a Hershel backpack pictured above from Urban Outfitters couple of days before leaving as my carry on.

![Stuff](images/20160320_182334.jpg)

I ended up ditching a lot of stuff from my previous blog post such as my boots. The picture above is what I ended up bringing. I'll go into more detail about what I packed in an upcoming blog post I promised regarding what I packed and lessons learned. (Spoiler alert: I over packed)

# Specs
![Specs](images/20160215_201144.jpg)

* Carry on luggage, 45L capacity
* Dimensions of 55x35x25cm comply with IATA cabin baggage limits
* Weight 1,500g
* Upper: (A) ATY nylon; or (B) waxed Kodra
* Base: 3x PU coating on 900 denier Kodra
* Padded laptop (15”)/ iPad compartment with self-ejecting mechanism
* External A4/ magazine pocket
* Quick access document organiser pocket for passports, ticket etc.
* Protective EVA padding throughout
* Internal water-resistant compartment for toiletries/ laundry etc.
* Backpack shoulder straps stored in the base
* Hide away adjustable shoulder strap on side
* Embossed lining
* Custom anodised aluminium tension lock and hook buckles
* Premium ‘seat belt’ nylon webbing
* Internal load compression straps with additional dry pocket

# What I loved
![love](images/20160215_201257.jpg)

* This bag was tested in the rain and the water resistance was working as advertised. 

* It had just the right amount of compartments and pockets. A lot of bags that I've tried before would over do the amount of pockets and compartments which tend to end up limiting the amount of stuff you are able to fit in the bag. The materials used to create dividers and pockets usually take up a lot of space in the bag itself and leaves a lot of dead space that's unusable.

* The two leak proof compartments in the bag was able to fit all my toiletries. I was able to ditch my doppel bag and save a lot of space in the main compartment.

* I was able to fit a lot of stuff into 45L of space.

* The bag can be used as a duffel with or without straps, or as a backpack with straps that can be tucked away.

* Very stylish looking.

# What I didn't like
![didn't like](images/20160318_002730.jpg)

* There is a pocket on the lid that was very roomy and could fit a lot of stuff, but putting too much stuff in here (roughly more than 50% full) would basically render your laptop compartment unusable which is also in the lid. I haven't tried whether a smaller laptop like a 13" Macbook air would improve this use case. But throughout the trip I would have trouble fitting every thing (including my hershel bag) into this bag. I ended up just putting my laptop in my hershel bag full time.

* Yes it can fit a lot into it and even stretches quite a bit to give you some breathing room in packing, but if it's at full capacity it can get quite heavy and backpack mode is highly uncomfortable on your back and shoulders. I ended up using the bag in duffel mode for most of my trip. Straps are pretty decently padded though and would be comfortable if the bag is not too heavy.

* The Duffel strap is uncomfortable. There really isn't much they can do about this. They had to put the duffel straps anchor points towards the center of the bag so it could be tucked away, but this caused the bags center of gravity to be weird and it would rock around quite a bit when walking with it in duffel strap mode. Also some padding on the strap would've helped but that would make it more difficult to tuck away.

* If you laid your bag down in duffel mode then switch over to backpack mode your back is basically touching the floor touching side. If you laid the bag down on a wet floor you basically won't be able to use it in backpack mode without getting your back wet. It's also quite dirty.

![Carrying](images/20160320_201920.jpg)
This was basically how I carried my stuff around the whole trip which is great assuming you didn't have a lot of walking to do. I was lucky that the only time I started hating myself for doing this was walking from the train station to our Ryokan in Arashiyama.

![Duffel Final](images/20160318_033104.jpg)

Here is a picture of what I ended up with in the end inside the duffel. For those who can't live off of 4 shirts and 2 pairs of pants for two weeks, this bag would be perfect for a long weekend trip. For those who needed something larger I highly recommend going with the [Osprey Farpoint 55](http://www.ospreypacks.com/us/en/product/farpoint-55-FARPNT55.html) that my girlfriend got for  the trip. I was seriously impressed with how well it performed. It can act as a duffel and backpack too and also has a detachable day pack in front, and it's designed to be way more comfortable on your back.

Overall I'm very happy with my purchase and I don't feel like I necessarily brought the wrong bag. I do feel like I could've packed less to make this bag work better, or got a bag similar to my girlfriends. I will be definitely keeping this bag and getting rid of my everlane weekender. I'm still tempted to pick up the [Minaal 2.0](http://www.minaal.com/) so I could compare the two.


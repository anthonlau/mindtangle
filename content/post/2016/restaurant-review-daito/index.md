+++
author = "Anthony"
date = 2016-03-14T14:09:13Z
description = ""
draft = false
slug = "restaurant-review-daito"
title = "Restaurant Review: Daito"

+++


Out of all the restaurants I've tried so far in Austin, Daito is by far my favorite. They specialize in homemade udon noodles with homemade dashi broth made from scratch (no dashi packet mix), this isn't something you find very often in the U.S. If you come here you'd definitely want to order a bowl of udon. You will find their broth to be heartwarmingly satisfying. 

Though they also serve good sushi with fish from Tsukiji Fish Market similar to Uchi and Uchiko. They keep their menu simple but still offer a good variety of similar dishes. You can't go wrong with ordering anything here.

The Kitsune Udon is my favorite udon from Daito. The name literally means Fox Udon originating from a folktale where foxes enjoy the deep fried tofu that is used as a topping for this dish. The tofu's texture manages to soak up just enough broth without becoming too soggy giving it a very unique texture similar to a soft smooth sponge that leaks out juicy broth.
![Kitsune Udon](images/20160126_195917.jpg)

Tempura Udon is another good choice if you are a fan of tempura and seeking something that's more familiar.
![Tempura Udon](images/20160126_200012.jpg)

Another favorite dish from Daito that I recommend everyone who comes here to try is the fermented/pickled mackerel sashimi roll. This is also home made and prepared a day in advance. The flavor could be hit or miss for some people but so far every one I convinced to try it has loved it.
![Mackerel](images/20160126_203039.jpg)

# Closing Thoughts

**The Good**

* Everything is home made and amazing
* Tsukiji market fish used for sushi
* Good selection of Sake

**The Not So Good**

* Place is kind of small and can get crowded at night

**The Bad**

* Nothing really, but if I had to complain about one thing it has to be parking, street parking in the surrounding neighborhood only and if you park across the street the closest crosswalk is kind of far.

If you live in Austin, and you haven't tried this place yet you should definitely pay them a visit.

**Presentation:** 3/5

**Quality:** 5/5

**Taste:** 5/5

**Price:** $20-$50 per person depending on whether you order sushi and/or alcohol

**Price to Quality Rating:** 5/5 Very reasonably priced for home making everything from scratch with amazing tastes.

Daito
2716 1/2 Guadalupe street
Austin, TX 78705
512-474-7770

Hours:
5 PM - 10 PM (Tuesday - Sunday)
Closed on Monday


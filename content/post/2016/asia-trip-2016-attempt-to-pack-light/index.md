+++
author = "Anthony"
categories = ["travel", "packing", "asia"]
date = 2016-03-21T00:14:09Z
description = ""
draft = false
slug = "asia-trip-2016-attempt-to-pack-light"
tags = ["travel", "packing", "asia"]
title = "Asia Trip 2016: Attempt to pack light"

+++


I tried to pack light for my NYC/Japan/Korea trip. Here is some pictures of my first attempts. I ended up taking out a few more things so I could put my day pack into the duffel bag to consolidate down to one bag if I wanted to.
![Items](images/20160317_235253-2.jpg)

* Outlier Slim Dungaree Pants
* Zanerobe drop shot olive pants
* Ice breaker Merino V-neck Tshirt
* Outlier Merino T shirt
* Mission Workshop Merino T shirt
* American Apparel Long sleeve Henley (Removed)
* Long sleeve flannel button down (removed)
* 1 black and 1 white scoop neck basic cotton T-shirt
* Uniqlo white OCBD
* Muji Merino wool scarf
* 3 travel silicone bottles
* Eagle creek compression cube (1 pair of uniqlo airism and 2 pairs of exofficio boxer briefs inside)

Here's everything packed into cubes with a doppler bag that I ended up getting rid of and just putting it into the compartments in my duffel bag.
![bag](images/20160318_002546.jpg)
![compartment](images/20160318_002721.jpg)
![compartment](images/20160318_002730.jpg)

Ended up having lots of extra room or so I thought, and I was considering bringing my boots but I figured I wouldn't be able to consolidate things into one bag if I brought them and ended up leaving them behind.
![packed](images/20160318_003035.jpg)
![final](images/20160318_033104.jpg)

I felt pretty good about my first attempt to one bag everything. But it wasn't until I arrived in NYC where I felt I might've not made the best choice. Most in part due to me packing last minute few hours before leaving for the airport due to prioritizing finishing up work. I wasn't able to take a final picture of everything I brought and packed since I was in a hurry. I'll make a follow up post on what I packed and lessons learned later on.


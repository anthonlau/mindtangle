+++
author = "Anthony"
categories = ["travel", "hong kong", "food"]
date = 2014-06-18T15:05:26Z
description = ""
draft = false
slug = "hong-kong-day-3"
tags = ["travel", "hong kong", "food"]
title = "Hong Kong Day 3"

+++


[Hong Kong Day 1](http://mindtangle.com/hong-kong-day-1/) <br/>
[Hong Kong Day 2](http://mindtangle.com/hong-kong-day-2/) <br/>
[Hong Kong Day 4](http://mindtangle.com/hong-kong-day-4/)

Woke up at around 9 am looking for food.  We tried desperately to look for a dim sum place around Tsim Sha Tsui but it seems like every thing was closed. It seems like all the places I used to eat at there has turned into high end and boutique clothing stores. I decided our best bet was to go to one of the restaurants in [Ocean Terminal 海運大廈](http://en.wikipedia.org/wiki/Ocean_Terminal,_Hong_Kong). Ocean Terminal is where cruises and US Navy ships usually dock.  This is also where you would take a boat ride to Macau.

![Victoria Harbor](images/1504255_10153700019125293_1786491034_o.jpg)
![ICC](images/IMG_20140101_104154.jpg)
The tall building is the [International Commerce Center (ICC)](http://en.wikipedia.org/wiki/International_Commerce_Centre)
![Ocean Terminal Outlook](images/1493561_10153700019405293_559667517_o-1-.jpg)
The area looking out into the harbour where we took the pictures
![Xmas Disney decoration](images/1560613_10153700019700293_857229042_n-1-.jpg)
Left over Christmas decoration from the Disney Store
![Eric Kayser Paris](images/1556455_10153700019840293_1233296347_o-1-.jpg)
The restaurant we wanted to go to was not open until 11am so we walked around and explored the terminal. We got a croissant from this bakery which was delicious.
![Toys R Us Pink](images/1507400_10153700019995293_387147011_o-1-.jpg)
![Star Wars Lego](images/IMG_20140101_105916.jpg)
![Star Wars Lego 2](images/IMG_20140101_105918.jpg)
We stopped by the Toys R Us. The Toys R Us in Hong Kong has a lot more interesting toys than the ones in America. They carry lots of japanese toys and gundam plastic models. I remember looking forward to visiting Toys R Us as a kid on the weekends. But after moving to the US, my first visit to the Toys R Us here was very disappointing.
![Thai Curry](images/IMG_20140101_112501.jpg)
Once 11am came we went to the restaurant by the boarding gates. Girlfriend had Thai Curry.
![Casserole](images/IMG_20140101_113200.jpg)
![Milk Tea](images/1502369_10153700020370293_2025778761_o-1-.jpg)
I had the shrimp casserole and of course HK style milk tea.
![MTR Walk and Save](images/IMG_20140101_122724.jpg)
Most of Hong Kong public transportation accepts the [Octopus Card 八達通 (means go everywhere/8 way pass)](http://en.wikipedia.org/wiki/Octopus_card). Growing up, I had to use the traditional magenetic strip cards that are similar to the NYC Metro card. We then switched over to the Octopus Card the same time NYC switched from token to the metro card. All we had to do now was keep the Octopus Card in our wallets and wave it over the scanner, much more convenient. At first only the bus and subway accepted it. Now you can use it for mini buses, at the super market, vending machines, convenience store and many other places.

In this picture, deep inside the terminal there's this stand where you scan your Octopus Card and it will take 2 HKD off your last fare. It's a MTR/Government initiative to get people to walk more and exercise more, and probably shop more.

![Muji](images/1522608_10153700020795293_214344954_o-1-.jpg)
Went to the Muji in the terminal and bought a scarf and sweater in preparation for the coldness in Japan.

![Star Ferry](images/IMG_20140101_130522.jpg)
The Star Ferry which carries commuters across the harbour between Hong Kong Island and Kowloon.

![More Xmas Decoration](images/IMG_20140101_130550.jpg)
![More Xmas Decoration2](images/IMG_20140101_130708.jpg)
More leftover Xmas Decoration

![Hong Kong Skyline 1](images/IMG_20140101_131037.jpg)
![Hong Kong Skyline 2](images/IMG_20140101_133310.jpg)
Pictures of the infamous Hong Kong Skyline on a hazy day. The news said that the smog in Beijing has been blown over to Hong Kong and Korea.

![Migrant Workers](images/IMG_20140101_133320.jpg)
Sunday is the designated day off for filipino migrant workers working as maids. They usually gather in Central on the ground floor of HSBC Hong Kong headquarters building. It basically looks like a huge picnic party going on with music and food every where pictured below. It seems like recently they have decided to start gathering by the harbour in Tsim Sha Tsui.
![Filipino](images/MDG-Migrant-workers-Filip-006.jpg)

![Clock Tower](images/1510976_10153700022215293_680718567_n-1-.jpg)
The Hong Kong Cultural Center and the clock tower.
![New Year party](images/IMG_20140101_133600.jpg)
Seems to be a New Year Party going on
![Slopes](images/IMG_20140101_133605.jpg)
The sides of the cultural center, as a kid I used to try and climb up them.

![Avenue of the Stars](images/1483400_10153700103225293_1534436235_n.jpg)
![Mainland Tourists1](images/IMG_20140101_134253.jpg)
![Mainland Tourists2](images/IMG_20140101_134255.jpg)
![Bruce Lee](images/1512153_10153700103720293_1008323330_o-1-.jpg)
![Bruce Lee Statue](images/1524060_10153700105745293_578385303_o-2-.jpg)
Avenue of the Stars was built to dedicate the many movie stars that came out of Hong Kongs film industry. It's kind of like hand prints at Hollywood. There was a sea of mainland tourists there. When it was first opened to the public, there wasn't a barrier around the Bruce Lee statue. I've also heard in the news that the walkway is sinking and in need of repairs soon.

![Star Ferry Terminal](images/IMG_20140101_140754.jpg)
Boarding the [Star Ferry](http://en.wikipedia.org/wiki/Star_Ferry) to Central in Hong Kong Island. It is a pretty cliche TV thing to do to be running down these ramps to make it to the last ferry of the night. The Star Ferry is a very old passenger ferry service across the harbour that was founded in 1888. They have encountered some financial problems recently and had to shut down some less popular routes. I'm glad they are still operational, since taking the ferry across the harbour is a much more relaxing / faster / less crowded way of getting to HK Island than taking the subway.
![Ferry](images/starferry.jpg)
![Ferry2](images/IMG_20140101_140815.jpg)
![Ferry3](images/IMG_20140101_140857.jpg)
More pictures of inside the boat.

![Cathedral](images/IMG_20140101_145258.jpg)
Once we stepped into Central it was like a totally different place with pedestrian walkways everywhere. We got a bit lost while trying to find our way to the Peak Tram and passed by this Cathedral in a Park.
![Very crowded applestore](images/1540406_10153700106725293_78031333_o-1-.jpg)
Super crowded apple store.

![Peak Tram Line](images/1519337_10153700107035293_1357885446_o-1-.jpg)
Waited in line for ~2 hours to take the [Peak Tram](http://en.wikipedia.org/wiki/Peak_Tram) up to the Peak.
![Tram1](images/IMG_20140101_155843.jpg)
![Tram2](images/IMG_20140101_160110.jpg)
![Wells](images/IMG_20140101_160143.jpg)
Doesn't look very steep when you look ahead but when you look to the side you'll see that it's actually VERY steep and the floor actually had wells in it to prevent you from sliding down.

![Mansions](images/IMG_20140101_163525.jpg)
![Peak View](images/IMG_20140101_162832.jpg)
Multi Million dollar mansions with one of the best view of Hong Kong. One of the more famous Hong Kong skyline pictures are taken here.

![Sunset](images/IMG_20140101_163521.jpg)
Sunset on the backside

Decided against waiting 2 hours for the Tram back down. Took the bus down instead and took the ferry back across to Kowloon.

![ICC Night](images/1492379_10153700109660293_1157957714_o-1-.jpg)
Night Time view of ICC

![Prada](images/prada.jpg)
Line outside Prada store on Canton Road. They seem to only allow 5-6 people in the store at once due to not trusting swarms of mainland tourists to enter the store all at once.
![Shoppers Lane](images/shopperslane.jpg)
Walked down Nathans Road to take the bus to get dinner at Victory Road. Passed by the shoppers lane where "high end" stores used to be. A lot of buses stop here.

![Bus TV](images/bustv.jpg)
Watching TV on the bus on the upper deck.

![Beef with egg suace](images/IMG_20140101_200926.jpg)
![hainanese Chicken](images/IMG_20140101_200932.jpg)
Went to [Victory Kitchen](http://www.openrice.com/english/restaurant/sr2.htm?shopid=10323&tc=sr1)
on Victory Road (pokemon jokes) for noms. I grew up eating here a lot since this is where I used to buy all my plastic model kits and toys until they slowly replaced the stores with restaurants and salons. I highly recommend this place if any of you visit HK. I had Beef with Egg Sauce and the girlfriend had [hainanese chicken](http://en.wikipedia.org/wiki/Hainanese_chicken_rice). This place is known for their hainanese chicken. The rice is turned yellow from the chicken fat and has A LOT of flavor in it (very unhealthy and I feel guilty every time I eat it).

Next post will be our last day in Hong Kong and exploring Mong Kok and Yau Ma Tei.


+++
author = "Anthony"
categories = ["food", "recipe"]
date = 2014-11-14T16:38:31Z
description = ""
draft = false
slug = "garlic-mashed-potatoes-recipe"
tags = ["food", "recipe"]
title = "Garlic Mashed Potatoes Recipe"

+++


I've been making this garlic mashed potatoes recipe for years now. It's my go to recipe for any sort of holiday dinners / potluck since it's simple to make and every one tends to bring some relatively fancy things with heavy flavors, and I like to balance it out with something lighter. 

**Note: this recipe does not include the gravy and I personally prefer it without gravy.*

### Ingredients
(portions are estimated and flavored to taste)

* ~8 Potatoes (4 Russet and 4 Red skin potatoes try to keep a 1:1 ratio)
* 1 stick of unsalted butter
* up to 1 pint of heavy cream (add more if you prefer the potatoes to be fluffier and softer)
* 1-2 heads of garlic
* Olive Oil
* Salt, Black Pepper, Garlic Powder
* Shredded Parmesan Cheese (Usually use 1/2 of those zip lock bags they sell in supermarkets)

### Tools

* Big pot big enough to fit all your potatoes and enough room to mash them without spilling out
* Something to mashed the potatoes with (if i don't have a masher I use a fork even though it hurts your hand after a while)
* Toaster Oven or Conventional Oven
* Baking sheet with aluminium foil

Lets begin.
1. Preheat oven to broil setting. Put aluminium foil over the baking sheet. Cut a head of garlic in half and put the flat sides on the baking sheet and pour olive oil over the garlic. Put it into the oven for 30 minutes to 1 hour depending on how blackened / intense you want the garlic flavor to be. When it's done turn off the oven and allow it to cool down. ![Garlic](images/IMG_20131128_142927.jpg)
2. Meanwhile, Peel the russet potatoes and leave the skin on the red potatoes. Cut them both into quarters or smaller. The smaller the pieces of potatoes the faster they cook. Put the cut up potatoes into the pot and fill it with water. Bring to a boil and leave it until the potatoes are cooked. I prefer to bring the water to boil with the potatoes in so the potatoes cook more evenly from outside to inside. ![Pot of potatoes](images/IMG_20131128_142818.jpg)
3. Drain the water but keep some water in the bowl since it contains starch and you can use it to thicken back up your mashed potatoes later just in case you added too much liquids and made it too watery. ![Drained Potatoes](images/IMG_20131128_145112.jpg)
4. Begin mashing the potatoes in the pot, once it breaks down enough to not be too chunky, add in the cheese, butter and heavy cream while it's still hot. Add the cream a bit at the time  and then mix it in to make sure you get the texture you want without it getting too watery/soft. If it gets too watery for your taste, add the starchy water we left over from earlier to thicken it up. ![Mashing the Potatoes](images/IMG_20131128_145258.jpg) ![Mashing the Potatoes2](images/IMG_20131128_145635.jpg)
5. Make sure the garlic is cool enough to the touch and use a papertowel to grab each half of the head of garlic one at a time, and squeeze out the soft insides into the mashed potatoes, it should leave most of the skin in the papertowel. ![Garlic complete](images/IMG_20131128_144456.jpg)
6. Add in salt, pepper and lots of garlic powder to flavor. Make sure to mix it well and give it a taste and add more if you think it needs more. ![Ingredients in](images/IMG_20131128_145852.jpg)
7. Keep adjusting the flavor and consistency until you get what you want and that's it. ![Right consistency](images/IMG_20131128_150430.jpg)


+++
author = "Anthony"
categories = ["travel", "hong kong", "food"]
date = 2014-11-24T16:17:24Z
description = ""
draft = false
slug = "hong-kong-day-4"
tags = ["travel", "hong kong", "food"]
title = "Hong Kong Day 4"

+++


[Hong Kong Day 1](http://mindtangle.com/hong-kong-day-1/) <br/>
[Hong Kong Day 2](http://mindtangle.com/hong-kong-day-2/) <br/>
[Hong Kong Day 3](http://mindtangle.com/hong-kong-day-3/)

Woke up to meet with girlfriends friend to have some [dim sum](http://en.wikipedia.org/wiki/Dim_sum). Went to one of the dim sum restaurants that I grew up going to with my grandfather on Sundays when I still lived in Hong Kong. [Ho Choi Seafood Restaurant](http://www.tripadvisor.com/Restaurant_Review-g294217-d776706-Reviews-Ho_Choi_Seafood_Restaurant-Hong_Kong.html)

Congee and rice noodle roll covering fried dough.
![Dim sum 1](images//IMG_20140102_111414.jpg)

Varius Baos, Phoenix Claws (Chicken feet), Congee and Ha Gao (shrimp dumpling).
![Dim sum 2](images//IMG_20140102_111900.jpg)

[Shu Mai](http://en.wikipedia.org/wiki/Shumai)
![Shu Mai](images//IMG_20140102_112658.jpg)

珍珠雞 Chun Chu Gai, a variant of [Lo Mai Gai](http://en.wikipedia.org/wiki/Lo_mai_gai) Glutinous Rice filled with Chicken and dried shrimp and sometimes eggs and other proteins wrapped in Lotus Leaf before steaming.
![Chun Chu Gai](images//IMG_20140102_112903.jpg)

We walked to a nearby shopping arcade and stores to shop for some gundam plastic models and kpop CD's.
![Gundam Plastic Modles](images//1531959_10153700119630293_740184675_o-1-.jpg)
![More plastic models](images//1496126_10153700119920293_1026878318_o-1-.jpg)

We then stopped by a chinese dessert chain called [Hui Lau Shan 許留山](http://en.wikipedia.org/wiki/Hui_Lau_Shan) who specializes in chinese desserts called tong sui which translates literally to sugar water. This chain was particularly known for its herbal teas and mango desserts. My personal favorite is the mango sago
![Dessert Menu](images//IMG_20140102_132745.jpg)
![Mango Sago](images//IMG_20140102_133431.jpg)
![Aloe Mango drink](images//IMG_20140102_133434.jpg)

On my [Hong Kong Day 2 post](http://mindtangle.com/hong-kong-day-2/), I mentioned [Wet Markets 街市](http://en.wikipedia.org/wiki/Wet_market). I decided to take the girlfriend and her friend to a public indoor government owned wet market. The government builds these wet market multi story buildings all around Hong Kong and rents it out to people to sell their produce and goods.
![Wet Market](images//HK_Yau_Ma_Tei_Market_-E6-B2-B9-E9-BA-BB-E5-9C-B0-E8-A1-97-E5-B8-82_Opening_Hours_-E7-94-98-E8-82-85-E8-A1-97_Kansu_Road-1-.jpg)

You can buy fresh fish and have it gutted the way you want it to be which has only been commonly available in the U.S. only recently. The meat are usually slaughtered elsewhere but I've seen it happen in the back before. You are also able to pick your caged chicken and they will slaughter / clean it up in the back. You usually pick a chicken then come back for it in 10-15 minutes. The cages for the chickens used to be wooden until the bird flu came along and the health department required everyone to switch over to metal ones.
![Fish](images//IMG_20140102_140951.jpg)
![Fish 2](images//1415039_10153700119890293_1096820800_o-1-.jpg)
![Fish 3](images//1506274_10153700120090293_966471573_o-1-.jpg)
![Produce](images//IMG_20140102_141318.jpg)
![Meat](images//IMG_20140102_141323.jpg)
![Chicken](images//93189117-chickens-in-cages-at-market-gettyimages-1-.jpg)

On the top floor of the wet market there's a [dai pai dong](http://en.wikipedia.org/wiki/Dai_pai_dong) style food court.
![Dai Pai Dong](images//IMG_20140102_141506.jpg)

Afterwards, we went to [Fa Yuen Street 花園街](http://en.wikipedia.org/wiki/Fa_Yuen_Street) A retail street containing stores that sells mostly boutique womens clothing and shoes and hawker stalls. We ran across one that was trying to advertise for a mop by putting tiles down on the street and spilling stuff on it. She had a headset on with loud speakers and everything.
![Fa Yuen Street 1](images//IMG_20140102_142108.jpg)
![Fa Yuen Street 2](images//IMG_20140102_142432.jpg)

Hong Kong has been building more and more pedestrian overpasses due to the sheer amount of people walking won't fit on the sidewalks anymore and it's also much safer (The overpass fills up during rush hour). We used the overpass nearby to walk towards the nearby [Grand Century Plaza](http://www.tripadvisor.com/LocationPhotoDirectLink-g294217-d3362366-i88440450-New_century_Plaza-Hong_Kong.html) Which is a mall built next to a train station. This place opened up when I was still young and was pretty depressing with not many things there. I was suprised at how things have changed and how much they have expanded and opened up many new stores. I got some french toast at the food court.
![Overpass](images//IMG_20140102_150839.jpg)
![Grand Century Plaza](images//IMG_20140102_160442.jpg)
![French Toast](images//IMG_20140102_155119.jpg)

We ended the day and had our last meal at Hong Kong by going back to Ocean Terminal in Tsim Sha Tsui to a burger place. It was a good burger, but definitely paying a premium for something that's common quality in the U.S.
![Burger](images//IMG_20140102_202928.jpg)

Last night in Hong Kong.
![Hong Kong Skyline 1](images//IMG_20140102_210023.jpg)
![Hong Kong Skyline 2](images//IMG_20140102_210029.jpg)
![Hong Kong Night Clock Tower](images//IMG_20140102_210640.jpg)
![Kowloon Park Pool](images//IMG_20140102_212302.jpg)

The next day we took the free airport express shuttle bus that takes us from the hotel to the nearest airport express train station.
![Airport Express Shuttle Bus](images//IMG_20140103_070307.jpg)
![Shuttle Bus](images//IMG_20140103_071438.jpg)

At the station, we can check in our luggage and get our boarding pass. Yes it's awesome! There are kiosks at the train station where you check in your bags and the bags gets loaded onto the train and gets handled at the airport directly! hassle free. The airport express is a bit expensive compared to the bus or taxi(when you have more than 2 people) but it gets to the airport in 30 minutes compared to 1 hour or more depending on traffic. Being able to check in your bags at the station is also hassle free and convinient. When arriving, attendants have carts neatly lined up for you to use.
![Airport Express](images//IMG_20140103_071812.jpg)
![Airport Express 2](images//IMG_20140103_072024.jpg)
![Airport Express 3](images//IMG_20140103_072602.jpg)
![To the airport](images//IMG_20140103_072713.jpg)
![Inside airport express](images//IMG_20140103_073324.jpg)
![Carts](images//IMG_20140103_075348.jpg)
![Airport Arrival Hall](images//IMG_20140103_075657.jpg)
![Prohibited Items 1](images//IMG_20140103_081304.jpg)
![Prohibited Items 2](images//IMG_20140103_081311.jpg)

People have been trying to bring back tons of baby formula from Hong Kong due to the [chinese baby formula scandal](http://en.wikipedia.org/wiki/2008_Chinese_milk_scandal)
![baby formula](images//IMG_20140103_081346.jpg)

More cool airport photos.

![Concourse](images//IMG_20140103_081435.jpg)
![Wonton Noodle](images//IMG_20140103_084453.jpg)
![Info Desk](images//IMG_20140103_085810.jpg)

USB Battery chargers were becoming pretty popular.
![USB Chargers](images//IMG_20140103_092202.jpg)

The Bathrooms were soooooo freakin clean. And instead of having toilet seat paper, you use some toilet paper and sanitizer to wipe the seat down.
![Toilet](images//IMG_20140103_093135.jpg)
![Toilet 2](images//IMG_20140103_093533.jpg)

Our gate and plane to Narita
![Gate](images//IMG_20140103_094046.jpg)
![Plane](images//IMG_20140103_095110.jpg)
![In plane entertainment](images//IMG_20140103_100928.jpg)

And this is the meal we had on the plane. Can't remember what it was though =\
![Meal](images//IMG_20140103_115326.jpg)

Next post will be about when we arrive in Japan.


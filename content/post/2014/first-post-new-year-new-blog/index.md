+++
author = "Anthony"
date = 2014-02-02T17:53:39Z
description = ""
draft = false
slug = "first-post-new-year-new-blog"
title = "First post, new year, new blog."

+++


Few of my friends have been telling me to start a blog for many years now.   I decided to mess around with Amazon Web Service's (AWS) EC2 for educational purposes and thought that hosting/writing a blog would be a good experience.  I'll also get a chance to brush up my writing skills since I'm a pretty terrible writer/speaker.  I tend to process my thoughts faster than I can piece together my words and end up making no sense at all or whoever is listening/reading will get lost.  Sometimes I get lost in my thoughts myself which is how I came up with the name of this blog, and I'm hoping that writing my thoughts out will help untangle the knots.  So here goes.

## Who are you?
Male in his 20's with a very curious mind.  I never have enough time to do/learn every thing I'm interested in. I'm currently a Consultant for a firm doing anything from Software Development to Systems Engineering among other things(Jack of all Trades). I can go on and on about my work and my life but I think there's enough content for a few posts in the future. I love to travel and learning new things about technology and gadgets.  I recently returned from a trip to Hong Kong and Japan and I'll be making posts documenting my trip.

## What are you going to write about?
Basically anything and every thing, but in short anything from just random quotes to food recipes I've tried out, traveling, technology, fashion and reviews of video games and anime.

## New Years Resolutions?
Since I was in Asia for new years I think this is a good time to set my goals for this year.

* ~~Write my first blog post~~ (I had this blog up since December but didn't write my first blog post until now)
* ~~Get my Security+ certification (Work requirement that I've been putting off for a few months now)~~
* Develop and design a theme for this blog
* Save enough money to travel abroad again
* Find a new job doing things I like out West.
* Post pictures and blog about my trip to Hong Kong and Japan.
* Learn either Ruby on Rails or Django and node.js
* Write an Android app.
* Get more proficient at Japanese.
* Try to blog at least once a week.

I think that's a list with a decent length with goals that are acheivable.


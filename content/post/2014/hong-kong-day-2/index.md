+++
author = "Anthony"
categories = ["travel", "hong kong", "food"]
date = 2014-03-28T04:19:14Z
description = ""
draft = false
slug = "hong-kong-day-2"
tags = ["travel", "hong kong", "food"]
title = "Hong Kong Day 2"

+++


[Hong Kong Day 1](http://mindtangle.com/hong-kong-day-1/) <br/>
[Hong Kong Day 3](http://mindtangle.com/hong-kong-day-3/) <br/>
[Hong Kong Day 4](http://mindtangle.com/hong-kong-day-4/)
### Blast to the past
We woke up and decided to pay a visit to where I used to live and pay a visit to my old primary and secondary school which was up the hill from where I lived.

![Perth Garden](images/1525580_10153700016080293_1517189913_n-1-.jpg)
Yes, this ugly pink building is still the same old color. For the first 5 years I lived here, it was a dull gray color.  Then they decided to renovate it and change it to this ghastly color.  I remember men climbing up the bamboo scaffolding and I'll look out the window and see them re-tiling the balcony where we kept our plants and herbs.

![Dunbar Road](images/MrGnYAZ.jpg)
The backside of my apartment building, forgot to take a picture of the front side which was Perth St.  Behind this wall was our community swimming pool.

![Bamboo](images/1487896_10153700016425293_826739607_o-1-.jpg)
Speaking of bamboo scaffolding, here's some laying on the side of the road. Hong Kong is famous for their use of bamboo scaffolding versus steel pipe scaffolding that americans use. 

![Bamboo Scaffolding](images/1502699_10153700017130293_668691154_o-1-.jpg)
![Bamboo Scaffolding2](images/1531897_10153700017135293_521039597_n-1-.jpg)
Bamboos are cheaper than steel, very strong, and grow super fast and reused many times.

![Bamboo building](images/390088839_6bd4c29612_o-1-.jpg)
Most of the super sky scrapers you see in Hong Kong's skyline are built with bamboo scaffoldings.  [Here's](https://www.youtube.com/watch?v=vwDIS_3RqxY) a youtube video about this ancient art.

![KJS](images/IMG_20131231_095846.jpg)
![KJS2](images/1498996_10153700016745293_751349926_o-1-.jpg)
Up the hill from my old apartment was my primary school.  I've heard back in 2011 that they were closing down.  I found out that they rebuilt it.  It's at least 4x taller than my old building. While I'm glad that they improved the building to accomadate for the increasing amount of students, it made me feel really old and part of my childhood has disappeared.

![KGV Path](images/NUMB6R2.jpg)
Up the hill farther was the path that I took to get to my secondary school.  They were closed for Winter Break and the guard wouldn't let us in so I couldn't get any pictures.  From the outside though it seems like not much has changed.

![Metro Park Hotel](images/1606229_10153700016905293_674574968_o-1-.jpg)
Down Waterloo Road, this is the Metropark Hotel, I usually stay here on my past trips to Hong Kong, very convinient location with many late night restaurants open.  On the right side is the [Cha chaan teng 茶餐廳](http://en.wikipedia.org/wiki/Cha_chaan_teng) where we had late lunch.

![Menu](images/5nok64O.jpg)
Their large menu selection.
![Milk Tea](images/VPq8CHP.jpg)
No trip to Hong Kong is complete without having [HK style Milk Tea](http://en.wikipedia.org/wiki/Hong_Kong-style_milk_tea)
![French Toast]()Also HK style french toast. Two thick slices of bread with the crust cutt of and sandwiched in peanut butter.  Dip it in egg then deep fry it.  Lather with syrup and butter on top.

### Wet Market
![Market](images/pnGz0ol.jpg)
![Market2](images/1495261_10153700017665293_95456597_o-1-.jpg)
![Market3](images/1491417_10153700017770293_1900187182_o-1-.jpg)
After lunch we walked through a nearby [wet market 街市](http://en.wikipedia.org/wiki/Wet_market) where people do their grocery shopping. In Hong Kong it's common to stop by one of these on the way home after work. These public markets are closer to farmer markets here in the U.S.  People tend to get fresh groceries for dinner every day. When I first moved back to the U.S. I found it hard to adjust to grocery shopping weekly.  I still go to the grocery store more often than I should nowadays. Later in our trip we paid a visit to one of the government run public wet markets which are indoors.

### Pharmacies in Hong Kong
Two of the biggest pharmacy chains in Hong Kong are [Mannings](http://www.mannings.com.hk/chi/) and [Watsons](http://www.watsons.com.hk/webhk/Home.do). But they are like the U.S. equivelent of CVS and Rite Aid. You would usually go to small private owned pharmacies to get your chinese herbal medicines and chinese herb prescriptions if you decided to see a chinese doctor.  One thing I forgot to stock up on was [White Flower Oil](http://www.amazon.com/White-Flower-Oil-0-67-Ounces/dp/B006EP7VAW) which is a great remedy for nausea and mosquito bites.
![Old Pharmacies](images/HK_Central_153_Wellington_Street_Wing_Hing_Dispensary_shop_Pharmacy_Sept-2012.jpg)Small pharmacies in Hong Kong used to look like this.

![New Pharmacies](images/1040129_10153700017985293_564118522_o-1-.jpg)
Nowadays you'll most of them located in prime areas with high rent looking like this.

I started noticing this trend when I last visited in 2011. One of my dad's friend explained to me it's due to the influx in mainlanders visiting Hong Kong to buy their groceries, medicine and of course baby formula.

I'm sure most of you have heard about the [2008 Baby formula scandal](http://en.wikipedia.org/wiki/2008_Chinese_milk_scandal). Six infants have died and 54,000 were hospitalized. Two owners of the company were given the death sentence. This has caused such a huge problem in Hong Kong because there was a shortage of baby formula and the price has shot up.  In fact at the airport I noticed that the customs are checking for baby formula now and they have imposed a limit on how much you are able to bring out.  I still noticed mainlanders loading up suit cases outside of stores with baby formula though.

Baby formula wasn't the only problem, I've heard of people using sewage water in soy sauce, and counterfeit of popular Chinese remedies/medicine killing people. ([Huge List of Food Safety Incidents in China](http://en.wikipedia.org/wiki/Food_safety_incidents_in_China)) Mainlanders were starting to look for medicine with the Made in Hong Kong label. But even then they can be fake.  So what better place to buy it than actually going down to Hong Kong?  People make a weekend trip out of going down to Hong Kong to shop for groceries, medicine, and for the wealthy there's the high end fashion stores.

![Mainlander](images/MxxoLDX.jpg)
A Mainlander and his shopping suitcase filled with his loot. It actually got quite annoying after a while when almost every one on the street is carrying a rolling suit cases and slowing traffic or they decide to lay their suitcase down and shuffle things around in the middle of the street.

Due to this increase in demand, these pharmacies started showing up every where in busy streets.  They have these flashy neon signs changing colors to try and attract tourists. My dad's friend has warned me though that due to high rent and increase in demand they will usually over charge 2x the price of normal pharmacies.  The girl friend and I actually found that it was easy to find at least 2-3 of these pharmcies in every other block.

### Mong Kok and Ladie's Market
![Mong Kok traffic](images/1520774_10153700018015293_473072544_n-1-.jpg)
[Mong Kok](http://en.wikipedia.org/wiki/Mong_Kok) is one of the most crowded places on earth. During lunch time and rush hour when all the high rise building unloads every one there's barely any place left to fit anyone on the  streets.  In fact after 5pm they close down certain roads for cars so that people can walk on it. 

![Ladies Market](images/1528678_10153700018115293_1152326604_n-1-.jpg)
![Ladies Market2](images/1098024_10153700018320293_1754228522_n-1-.jpg)

[Ladies Market 女人街 (Literally translated as Ladies Street)](http://www.discoverhongkong.com/eng/see-do/highlight-attractions/top-10/ladies-market.jsp) is located at the heart of Mong Kok. It's actual name is Tung Choi Street 通菜街. It got it's name ladies market because of all the bags and clothing you can buy there even though there's plenty of things for children and men too (Watches, Sunglasses, stores selling cute things similar to [Morning Glory](https://www.google.com/search?q=morning+glory&rlz=1C1CHFX_enUS575US575&oq=morning+glory&aqs=chrome..69i57j0l5.1040j0j9&sourceid=chrome&espv=210&es_sm=93&ie=UTF-8)) . The girl friend got very irritated by the crowd even though we went in the middle of the day when everyone's at work so it's already considered not very crowded. I remember hating this place while growing up cause my Mom and my sister's would drag me there and being the small kid I am I would get constantly knocked over and pushed around. It is also not very comfortable if you go during the crowded weekend in the middle of a humid Hong Kong summer.

![Langham Place](images/langham.jpg)
![Langham Place 2](images/langham2.jpg)
Years ago [Langham Place](http://en.wikipedia.org/wiki/Langham_Place_(Hong_Kong)) was built behind my grandparents' old office building in Mong Kok. The neighborhood where they created and built up their business was gone and replaced with this gigantic mall/hotel. 
![bla bla bra](images/1559465_10153700018285293_1429940703_o-1-.jpg)
![uuushooop](images/1511556_10153700018510293_1539869935_o-1-.jpg)
![namco arcade](images/1517585_10153700018700293_1543557375_n-1-.jpg)
![12](images/1531975_10153700018695293_1388119299_o-1-.jpg)
![ginger](images/1017522_10153700018815293_642697475_n-1-.jpg)
![Hello Kitty](images/1074695_10153700019105293_737694529_o-1-.jpg)
![CD Store](images/1489587_10153700019100293_450177571_o-1-.jpg)
There was 15 levels of shopping to be done here and a big food court with an Ippudo, Mos Burger, Starbucks etc.

### Dinner
![Cha Siu Fan 叉燒飯](images/8Dcm7mz.jpg)
Girl friend had [Cha Siu](http://en.wikipedia.org/wiki/Char_siu) Fan 叉燒飯 (BBQ Pork over rice)
![Siu Ngou Fan 燒鵝飯](images/0EPPzkT-1.jpg)
I had the [Siu Ngou](http://en.wikipedia.org/wiki/Roast_goose) Fan 燒鵝飯 (Roasted Goose over rice)

The two dishes above are classified as [Siu Mei 燒味](http://en.wikipedia.org/wiki/Siu_mei). Siu Mei over rice is a very popular take out dish. Many people also order the Siu Mei to take home to celebrate events or if they are simply too lazy to cook. You can get Cha Siu in U.S. but it just doesn't taste the same. I opted for the Goose because you can't get that in the U.S. for some reason. It was late and the girl friend wasn't feeling well so we didn't want to travel far. This place was close to our hotel which was quite expensive for the small portions we got, the quality was also medicore by Hong Kong standards. When ordering Char Siu, most people prefer to have it half fatty and half lean.

At this point in the trip, I've noticed that we've been having trouble finding local small non chain places to eat dinner that isn't open late. This might only be true for the Kowloon Area since we didn't really spend much time in Hong Kong Island. But I definitely noticed a difference from when I lived there. I might touch upon this subject on later posts.


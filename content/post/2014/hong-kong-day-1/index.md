+++
author = "Anthony"
categories = ["travel", "hong kong", "food"]
date = 2014-03-20T06:45:34Z
description = ""
draft = false
slug = "hong-kong-trip-day-1"
tags = ["travel", "hong kong", "food"]
title = "Hong Kong Trip Day 1"

+++


[Hong Kong Day 2](http://mindtangle.com/hong-kong-day-2/)<br/>
[Hong Kong Day 3](http://mindtangle.com/hong-kong-day-3/)<br/>
[Hong Kong Day 4](http://mindtangle.com/hong-kong-day-4/)

I've said that I would try to blog once a week but I have been pretty bad at it so far. But I've decided to try and follow the rules of ["No More Zero Days"](http://i.imgur.com/B3EgTLe.jpg) and forgive my past self.

Day 1-3 of my Hong Kong trip was short, due to day 1 and 2 being the time difference + time spent flying on the plane and Day 3's whole morning was spent waiting for our luggage to be delievered to our hotel because our luggage didn't make it onto the plane with us, I'll be referring to these days as Day 1 of our trip.

Our flight from DC to JFK was delayed, and we managed to run from one terminal to the other, check-in again, go through security again, and run to the gate just in time for final boarding. (I'll probably write a post some other time about how terrible U.S airport designs are).

### The flight
I insisted on flying Cathay Pacific even though it was another 400 dollars more. I go back to Hong Kong every 3 years since I left and the lesson I've learned is to not fly any American airlines. There's less leg room, the planes are older and every time you fly them you're basically rolling the dice to see if you end up with the old plane with the community shared 17" TV stuck in the ceiling.  When I went back in 2011 I flew United, and being a 6'4" tall I spent most of the 15 hour flight standing at the rear of the plane. The food was also terrible compared to airlines like Cathay, Korean Air, Virgin etc.

![Plane Menu](images/IMG_20131228_135305.jpg)
Here is the menu for the flight.

![Chicken](images/IMG_20131228_152508.jpg)
I had the chicken.

![Pasta](images/IMG_20131228_152519.jpg)
The girlfriend had the pasta.

![Sweet'n Sour pork](images/IMG_20131228_223947.jpg)
Sweet and sour pork I had for second meal. Girlfriend fell asleep and didn't eat.

![Arrival Bus](images/IMG_20131229_202633.jpg)
We decided to take the bus instead of the air train due to the cheaper price and less transfer required. Since we were exhausted we didn't want to deal with transfering trains. Since I was so tired I forgot to take pictures of the airport when we arrived and the scenery during the ride into the more populated part of Hong Kong.

### Next morning
![Snacks](images/IMG_20131230_081728-2.jpg)
We were starving the next morning, but didn't wanna miss the delivery of our luggage from the airport so I ran across the street to the 7Eleven and some of my favorite child hood snacks. From the left: chrysanthemum tea, lemon tea and bbq beef corn flavored chips (Girlfriend and I almost fought to the death over the last few pieces, they were that good)
![BabyStarSnack](images/IMG_20131230_095339-1.jpg)
![babyStarSnack2](images/IMG_20131230_095353.jpg)

![Wonton Noodles](images/IMG_20131230_124040-1.jpg)
After we got our luggage, we walked around Tsim Sha Tsui and settled on having lunch at a pretty famous (and over priced if you ask me) Wonton Noodle Soup chain called [Chi Kee](http://www.openrice.com/english/restaurant/sr2.htm?shopid=363)

![MTR](images/IMG_20131230_131747.jpg)
![MTR2](images/1523916_10153700009625293_910763219_o.jpg)
![Tung Chung Station](images/IMG_20131230_135322.jpg)
We decided to take the MTR and visit the sitting buddha statue at Tung Chung.

![Cable Car](images/IMG_20131230_155352.jpg)
![Cable Car View](images/IMG_20131230_155359.jpg)
![Cable Car View 2](images/IMG_20131230_160901.jpg)
We took the [Ngong Ping 360](http://www.np360.com.hk/en/) cable car up to where the buddha statue is.(Apparently this is a relatively new thing)  I forgot to take a picture of the huge line we were waiting in for ~2.5 hours. It was around new years time so a lot of people wanted to go up there to wish for good luck / ask for their fortune.

![Cable Car View 3](images/IMG_20131230_160218.jpg)
![Trail Runners](images/1523756_10153700011465293_1539807540_o.jpg)
There was a path below us for hikers. People were running along it. Apparently trail running is pretty popular in Hong Kong with the numerous amounts of trails they have.

![Grave stone](images/NWzeQWl-1-.jpg)
Gravestones in the hills.

![Village](images/1523756_10153700011465293_1539807540_o-3.jpg)
![Ngong Ping](images/1523756_10153700011465293_1539807540_o-2.jpg)
![Sign](images/1523756_10153700011465293_1539807540_o-4.jpg)
The little shopping area and restaurants they built for tourists at the top.

![Street food](images/RdcmYaf.jpg)
Street food vendor selling sausages, fishballs and squid on skewers.

![Sign](images/YWuZWmJ.jpg)
![Buddha](images/BFQQpJp-1.jpg)
![Buddha 2](images/ofvsf0k.jpg)
It was pretty difficult to get a decent picture due to the positioning of the sun and I'm taking pictures with a camera phone. They claim that this is the biggest sitting bronze buddha statue in the world.

![Cow](images/Wn3rE4B.jpg)
As we were walking back to take the tram down saw a random cow going through the rubbish bin.
![front door](images/uHzu3sP.jpg)

![Random Ramen store](images/zw0DUbm.jpg)
Went by a random ramen shop at the outlet mall at the train station. Decided to wait until we get back to Tsim Sha Tsui to grab food.

![Dinner](images/gaUuIOA.jpg)
We took a nap and ended up oversleeping and most restaurants nearby were closed. We didn't want to take the train or bus so we went into a nearby McDonald's and had this.  Yes.... Hong Kong Mcdonalds had Katsu Curry. (Fried Pork Cutlet and Curry over rice). It was suprisingly good, if I had to complain about anything it would have to be the instant rice they used tasting weird. Not to mention, this came out to around $2 USD.

![Neon signs](images/radPysw.jpg)
I'll end with a short preview of all the neon signs we were about to see.


+++
author = "Anthony"
categories = ["fashion", "rant", "opinion"]
date = 2014-02-22T09:15:42Z
description = ""
draft = false
slug = "why-i-probably-wont-shop-at-uniqlo-again-and-hm"
tags = ["fashion", "rant", "opinion"]
title = "Why I probably won't shop at Uniqlo again (and H&M)"

+++


![Uniqlo Logo](images//2000px-UNIQLO_logo-svg-1-.png)

A bit of a background for those who don't know what [Uniqlo](http://www.uniqlo.com/us/home) is. Uniqlo is a Japanese casual wear designer, manufacturer and retailer. Think GAP, but lower price, higher quality and a bit less boring.


Uniqlo used to only have one location in the US, which is SoHo, NYC. When I lived in New York I felt privileged to have access to this store which most other Americans on reddit complained about not having. In fact, on many fashion forums many people were offering **proxy** services *(buy requested items then ship it to them)* up until uniqlo launched their online store back in October 2012.  For the first year and a half after I moved away from New York, Uniqlo would be an important part of my agenda on trips back to New York *(the others being eating ramen, getting a haircut and playing handball)*.

I'm 6'4" (193cm) tall, broad shoulders and skinny waist, it's pretty difficult to find clothes in America that doesn't automatically assume you are over weight at those sizes. Pants that are barely long enough, if I wanted my jeans to stack a bit, at that length all the waist sizes are 40" plus. For the upper body, most things are either too tight around the shoulder or too baggy around the waist. My only real options were to buy things then get them tailored, get things custom made or buy higher end designer brands for [~$75](http://www.us.allsaints.com/men/t-shirts-and-polos/style,any/colour,any/size,any/) a shirt. Until I found Uniqlo (and later H&M). Uniqlo shirts were what they would call slim fit now, they fit mee very well without having the paper T shirt quality like Hanes or H&M. That was until my last trip to New York.

![uniqlo store japan](images//Uniqlo-UT-T-Shirts-2012-055.jpg)
Uniqlo store in Japan carries a lot more varieties, in the U.S. they would do collaboration with famous designers or anime and offer more interesting clothing for a limited time.

On my last trip to Uniqlo, I noticed a few of their hoodie's quality seem to have went down a bit, the fits on their shirt also seemed to have gotten wider around the waist for men. I thought it was just me, but found out later that it wasn't just me. My sister did end up buying some heat tech gear and basic shirts. Once we got back home my sister noticed something was weird with the fit of her shirt, she pulled out her old shirt and did a comparison. The shirt on the bottom is the old one, the shirt on top is the new one. Yes they are the same size.
![Uniqlo shirt comparison](images//download_20131123_152648.jpg)
I'll include an email my sister sent to Uniqlo since she can probably describe what the problem is better than I can and it shows how disappointed she is.

    Hi Uniqlo,

    I have been a faithful customer of yours since before you came to the
    States. I was a particular fan of your premium cotton washed t shirts
    - they were lightweight, soft, last forever, and came in great colors.
    I live in Washington, DC, so whenever I was in Japan, Hong Kong, or
    New York, I would make it a point to stop at a Uniqlo.
    
    Last week I went to your 5th Avenue store and spent $150 on your
    washed cotton tees, in addition to a few heat tech items. I didnt
    even bother to try the t shirts on because I knew my size. Or so I
    thought. Below is a photo comparing the old t shirt cut (grey) and the
    new t shirt cut (pink). Not only are the new shirts a full 3 shorter,
    but the necklines are so wide that I will have to wear another shirt
    underneath to stay decent. I cant tell you how disappointed I am.
    
    The quality of the material, thank goodness, seems to be the same, but
    if you are looking to cut costs by cutting a few inches off each
    shirt, then you are taking the wrong approach. Unfortunately I did not
    try on the shirts until after I returned to DC, so I have no hope of
    returning them now, but please, I beg you to return to the old cut.
    Its impossible to find high quality cotton t shirts that are long
    enough to wear (especially with all the low-rise pants these days) at
    a decent price and with a selection of colors. Until you do, I will go
    back to shopping at the Gap and J Crew.
    
    Sincerely,
    
This was their response.

    Thank you for contacting the UNIQLO USA Customer Center.

    We do sincerely apologize for any inconvenience this may have caused. We appreciate your feedback.
    
    You are a valued customer and we appreciate your business. For inquiries and any other concerns please do not hesitate to call Customer Center at 1-855-4UNIQLO(1-855-486-4756) or email wecare@mail.uniqlo-usa.com. We would like to thank you for choosing UNIQLO.
    
    Sincerely,
    Misha

Both my sister and I were disappointed by their automatic response that basically says "too bad I don't care".

## So why has it changed?
I can't say for sure, but recently  Uniqlo has been expanding and growing very fast in the U.S. due to surge in popularity I mentioned above. They opened around 17 stores in the past 2 years and plan to open [more](http://www.uniqlo.com/us/uniqlo-news/2014/uniqlo-announces-the-opening-of-five-new-stores-for-spring-summer-2014/). The first two new stores that they opened were in both in Manhattan (in addition to the existing SoHo one). This shows how many people want to buy their clothes.

My guess is that with this many stores opening, they need to increase their supply and cut costs where they can. In my sisters case cutting off a couple of inches from every shirt saves a lot of material. By making the mens shirt wider around the waist they will fit more of your average American. And according to one of my friends, slim fit shirts means cutting more fabric around the waist which equals more labor cost, and it's not the same as cutting off the bottom of the shirt where it's just a straight cut that can be probably done by a machine. H&M has also been becoming more popular recently and opening more stores, I started noticing a change in cut on their pants and button down shirts. I noticed their pants got a lot baggier and their dress shirts felt boxier compared to my old H&M clothes. I ended up returning most of my recent purchases.

I haven't bought any shirts or pants for a while now, mainly because I work from home most of the time and never really go out except to run errands so there's no reason for me to dress up. But if I had to replace my wardrobe, I'd probably try out Zara (owned by same company that owns H&M I think). They seem to be retaining their slim cuts, though they are a lot more expensive. Either that or I'll have to start venturing into the world of designer fashion.

